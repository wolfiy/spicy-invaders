var indexSectionsWithContent =
{
  0: "._abcdefghilmnoprstuvwxy",
  1: "abcefghimpsu",
  2: "c",
  3: ".abcdefghimpsu",
  4: "abcdefghilmoprstuw",
  5: "_abdeghilmnopstw",
  6: "dems",
  7: "aeghilmnprsuw",
  8: "cdefgilmopstvwxy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties"
};

