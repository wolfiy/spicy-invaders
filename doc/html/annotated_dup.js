var annotated_dup =
[
    [ "Ch", "namespace_ch.html", [
      [ "Etml", "namespace_ch_1_1_etml.html", [
        [ "IO", "namespace_ch_1_1_etml_1_1_i_o.html", [
          [ "FileManager", "class_ch_1_1_etml_1_1_i_o_1_1_file_manager.html", "class_ch_1_1_etml_1_1_i_o_1_1_file_manager" ]
        ] ],
        [ "Play", "namespace_ch_1_1_etml_1_1_play.html", [
          [ "Game", "namespace_ch_1_1_etml_1_1_play_1_1_game.html", [
            [ "Controllers", "namespace_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers.html", [
              [ "GameController", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_game_controller.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_game_controller" ],
              [ "MenuController", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_menu_controller.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_menu_controller" ],
              [ "SoundController", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_sound_controller.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_controllers_1_1_sound_controller" ]
            ] ],
            [ "Engine", "namespace_ch_1_1_etml_1_1_play_1_1_game_1_1_engine.html", [
              [ "Constants", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants" ],
              [ "Helper", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_helper.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_helper" ],
              [ "IDestructible", "interface_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_i_destructible.html", "interface_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_i_destructible" ],
              [ "IUpdatable", "interface_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_i_updatable.html", "interface_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_i_updatable" ]
            ] ],
            [ "Entities", "namespace_ch_1_1_etml_1_1_play_1_1_game_1_1_entities.html", [
              [ "Alien", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_alien.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_alien" ],
              [ "Bunker", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_bunker.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_bunker" ],
              [ "Enemy", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_enemy.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_enemy" ],
              [ "EnemyContainer", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_enemy_container.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_enemy_container" ],
              [ "GameObject", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_game_object.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_game_object" ],
              [ "Missile", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_missile.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_missile" ],
              [ "Spaceship", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_spaceship.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_spaceship" ],
              [ "Squid", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_squid.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_squid" ],
              [ "Ufo", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_ufo.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_entities_1_1_ufo" ]
            ] ],
            [ "Models", "namespace_ch_1_1_etml_1_1_play_1_1_game_1_1_models.html", [
              [ "GameModel", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_game_model.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_game_model" ],
              [ "MenuModel", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_menu_model.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_menu_model" ],
              [ "ScoreModel", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_score_model.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_score_model" ],
              [ "SoundModel", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_sound_model.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_models_1_1_sound_model" ]
            ] ],
            [ "Views", "namespace_ch_1_1_etml_1_1_play_1_1_game_1_1_views.html", [
              [ "BaseMenuView", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_base_menu_view.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_base_menu_view" ],
              [ "GameView", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_game_view.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_game_view" ],
              [ "MainMenuView", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_main_menu_view.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_main_menu_view" ],
              [ "PauseMenuView", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_pause_menu_view.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_pause_menu_view" ],
              [ "SettingsMenuView", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_settings_menu_view.html", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_views_1_1_settings_menu_view" ]
            ] ]
          ] ],
          [ "Program", "class_ch_1_1_etml_1_1_play_1_1_program.html", "class_ch_1_1_etml_1_1_play_1_1_program" ]
        ] ],
        [ "Utils", "namespace_ch_1_1_etml_1_1_utils.html", [
          [ "Console2", "class_ch_1_1_etml_1_1_utils_1_1_console2.html", "class_ch_1_1_etml_1_1_utils_1_1_console2" ],
          [ "Math2", "class_ch_1_1_etml_1_1_utils_1_1_math2.html", "class_ch_1_1_etml_1_1_utils_1_1_math2" ]
        ] ]
      ] ]
    ] ]
];