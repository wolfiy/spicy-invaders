var class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants =
[
    [ "readonly", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a5a67bb50b7f5994b84cb87eb6ad297a5", null ],
    [ "readonly", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a82d9cde3feb797c9066156a63ca96865", null ],
    [ "readonly", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a7749841c70aec9c1edeb36a887d7888c", null ],
    [ "ABOUT_TITLE", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a76db3a1fe51eddb47f5348c559866274", null ],
    [ "BUNKER_HEIGHT", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a1acc2757081259d70c48b7957cd2d0b9", null ],
    [ "BUNKER_POSITION_Y", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a00d3f38f649661bbf5f17397ea1fa6cd", null ],
    [ "BUNKER_WIDTH", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a47f4eae7f5b631ab0f411bf180800c5e", null ],
    [ "BUTTON_WIDTH", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a498934ae2672220e04c0d4c26932ab98", null ],
    [ "DEFAULT_ENEMY_WIDTH", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#ad5123c6d1a9204e508a95fb5c5a279dd", null ],
    [ "ENEMY_SPACING", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a7eafd1460cc43899db99d963626b0bd7", null ],
    [ "ENEMY_THRESHOLD", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a791c093ccc0a79deed59c9f806b6e09b", null ],
    [ "GAME_OVER", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a9af388ce06c82450e44f8b2aeb967b48", null ],
    [ "HIGH_SCORES_DESCRIPTION", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#af6843df6d2506340bac3e6e913d1d5ab", null ],
    [ "LIVES_SPACESHIP", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a8b84c312a004ddfca0d303b50cb31686", null ],
    [ "MAIN_DESCRIPTION", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a826146b59db25a9b53cb7c95d5d316cb", null ],
    [ "MAIN_TITLE", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a38e51942ddd1344c5315e9ad3496e2eb", null ],
    [ "MARGIN", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#aafc12027c41c1aef97502a909c8456f4", null ],
    [ "PAUSE_TITLE", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a05b54c45d14dd8ecde1658d8e0bbeb53", null ],
    [ "PLAYABLE_BOUND_LEFT", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#ade28ddf56d6c604f268f90533aaa3b23", null ],
    [ "PLAYABLE_BOUND_RIGHT", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a25611a3098c127273de1f2533999cd1d", null ],
    [ "SCORE_ENEMY", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#aa167d305c19e175e68fbf1f1359b9f95", null ],
    [ "SETTINGS_MENU", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#aafa4bcddbb574d90246b24a0cf589a16", null ],
    [ "SPACER", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a1c725200ec88b37a244d7fa2f9ef5c79", null ],
    [ "SPRITE_SPACESHIP", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a566d25016f3fda2ba58edf0c29d32f9c", null ],
    [ "TITLE_SCORES", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#acc62a44646273830592c89e4746d8cec", null ],
    [ "WINDOW_HEIGHT", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#a4615e4767358d0a1db90a68be343d03c", null ],
    [ "WINDOW_WIDTH", "class_ch_1_1_etml_1_1_play_1_1_game_1_1_engine_1_1_constants.html#aeb11b696e7ba949989010678b75ed4b2", null ]
];