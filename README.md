# Spicy Invaders
A CLI based Space Invaders clone. Part of the P_Dev class at ETML

## Installation
You can grab a portable version of Spicy Invaders from the 
[releases](https://gitlab.com/wolfiy/spicy-invaders/-/releases) page. There, 
a `.msi` installer and a `setup.exe` files can be found. Use wither to install 
the game to the program files directory.

## Building
Spicy Invaders was made in C#, using .NET Framework 4.7.2.

## Features
### Difficulty
The game can be played in easy, medium or hard difficulties. 

On easy mode, only the bottom most row of enemies can shoot missiles. Also, when
enemies reach the bottom of the screen, they keep moving from left to right
letting the player take their time to eliminate the remaining invaders.

On medium difficulty, the player loses once the enemies have reached the bottom.
Finally, on hard difficulty, all enemies can shoot at the same time.

### Sound
The game provides sound effects and background music.

### High scores
Highscores are recorded and stored in a text file. In game, only the top 15
scores are displayed but the file keeps track of all scores.

### Framerate
The game provides adjustable framerate.

## Documentation
Proper documentation will be provided with the final release of the game.

## Class diagram
![class diagram](report/figures/SpicyInvadersClassDiagram.png)