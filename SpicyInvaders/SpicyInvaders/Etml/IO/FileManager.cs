﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 14th, 2024

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleToAttribute("SpicyInvaders.Tests")]

namespace Ch.Etml.IO
{
    /// <summary>
    /// Manages input / output with the file system.
    /// </summary>
    public class FileManager
    {
        /// <summary>
        /// Index of the username in a scores file line.
        /// </summary>
        private const int INDEX_OF_USERNAME = 0;

        /// <summary>
        /// Index of the score in a scores file line.
        /// </summary>
        private const int INDEX_OF_SCORE = 1;

        /// <summary>
        /// Index of the wave in a scores file line.
        /// </summary>
        private const int INDEX_OF_WAVE = 2;

        /// <summary>
        /// Number of scores to display in the high scores menu.
        /// </summary>
        private const int NB_SCORES_SHOWN = 15;

        /// <summary>
        /// Character used to separate username, score and wave in the score 
        /// file.
        /// </summary>
        private const char SEPARATOR = ',';

        /// <summary>
        /// Path of the score file.
        /// </summary>
        private readonly string _path;

        /// <summary>
        /// Initializes a new <see cref="FileManager"/>.
        /// </summary>
        /// <param name="path">Location of the score file.</param>
        public FileManager(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Writes a score to the scores file. A score in the scores file
        /// follows the "username,score,wave" pattern.
        /// </summary>
        /// <param name="username">Name of the player.</param>
        /// <param name="score">Score achieved by the player.</param>
        /// <param name="wave">Wave achieved by the player.</param>
        public void WriteScore(string username, int score, int wave)
        {
            using (StreamWriter writer = File.AppendText(_path))
            {
                var line = $"{username},{score},{wave}";
                writer.WriteLine(line);
            }
        }

        /// <summary>
        /// Gets the 15 best scoresfound in the scores file.
        /// </summary>
        /// <returns>A list of the 15 highest scores, ordered from best to 
        /// worst.</returns>
        public List<(string, int, int)> GetScores()
        {
            List<(string username, int score, int wave)> scores
                = new List<(string, int, int)>();

            using (StreamReader reader = new StreamReader(_path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(SEPARATOR);
                    scores.Add((line[INDEX_OF_USERNAME], 
                                int.Parse(line[INDEX_OF_SCORE]), 
                                int.Parse(line[INDEX_OF_WAVE])));
                }
            }

            return scores.OrderByDescending(s => s.score)
                         .Take(NB_SCORES_SHOWN)
                         .ToList();
        }
    }
}
