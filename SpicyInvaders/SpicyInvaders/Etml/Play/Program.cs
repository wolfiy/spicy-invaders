﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 22nd, 2024

using System;
using System.Runtime.InteropServices;
using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Play.Game.Views;

namespace Ch.Etml.Play
{
    /// <summary>
    /// Entry point of the program.
    /// <see cref="https://stackoverflow.com/questions/38426338/"/> for details
    /// about the removing of buttons.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Empty command.
        /// </summary>
        private const int MF_BYCOMMAND = 0x00000000;

        /// <summary>
        /// Address of the minimize button.
        /// </summary>
        public const int SC_MINIMIZE = 0xF020;

        /// <summary>
        /// Address of the maximize button.
        /// </summary>
        public const int SC_MAXIMIZE = 0xF030;

        /// <summary>
        /// Address of the resizing cursor.
        /// </summary>
        public const int SC_SIZE = 0xF000;

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        /// <summary>
        /// Sets up the console and starts the game.
        /// </summary>
        /// <param name="args">Launch arguments.</param>
        [STAThread]
        static void Main(string[] args)
        {
            // Getting handle and system menu of the console window
            IntPtr handle = GetConsoleWindow();
            IntPtr sysMenu = GetSystemMenu(handle, false);

            // Removing buttons if valid
            if (handle != IntPtr.Zero)
            {
                DeleteMenu(sysMenu, SC_MINIMIZE, MF_BYCOMMAND);
                DeleteMenu(sysMenu, SC_MAXIMIZE, MF_BYCOMMAND);
                DeleteMenu(sysMenu, SC_SIZE, MF_BYCOMMAND);
            }

            // Setup console
            Console.CursorVisible = false;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.SetWindowSize(Constants.WINDOW_WIDTH,
                                  Constants.WINDOW_HEIGHT);
            Console.SetBufferSize(Constants.WINDOW_WIDTH,
                                  Constants.WINDOW_HEIGHT);

            // Start Spicy Invaders
            Start();
        }

        /// <summary>
        /// Sets all components up and opens the main menu.
        /// </summary>
        public static void Start()
        {
            // Models
            GameModel gameModel = new GameModel();
            MenuModel menuModel = new MenuModel();
            SoundModel soundModel = new SoundModel();

            // Views
            GameView gameView = new GameView();
            MainMenuView mainMenuView = new MainMenuView();
            PauseMenuView pauseMenuView = new PauseMenuView();
            SettingsMenuView settingsMenuView = new SettingsMenuView();

            // Controllers
            MenuController menuController = 
                new MenuController(menuModel, mainMenuView, 
                                   pauseMenuView, settingsMenuView);
            GameController gameController = new GameController(gameModel, gameView);
            SoundController soundController = new SoundController(soundModel, gameModel);

            // Link controllers
            gameController.MenuController = menuController;
            gameController.SoundController = soundController;
            menuController.GameController = gameController;
            menuController.SoundController = soundController;

            // Open main menu
            menuController.OpenMainMenu();
        }
    }
}
