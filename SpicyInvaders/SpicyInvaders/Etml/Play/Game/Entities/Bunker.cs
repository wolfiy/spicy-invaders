﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 3rd, 2024

using Ch.Etml.Play.Game.Engine;
using System;

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents an in-game bunker. A bunker consists of a single cell. To
    /// form a wall, use a bunch of bunkers. The 
    /// <see cref="Game.SpawnBunkers(int, int, int, int)"/> allows for easy
    /// creation of walls.
    /// </summary>
    internal class Bunker : GameObject, IDestructible
    {
        /// <summary>
        /// Number of lives of a bunker.
        /// </summary>
        private const int LIVES = 2;

        /// <summary>
        /// Console representation of a bunker.
        /// </summary>
        private const string SPRITE = "▓";

        /// <summary>
        /// Console representation of a damaged bunker.
        /// </summary>
        private const string SPRITE_DAMAGED = "░";

        /// <summary>
        /// Initializes a new <see cref="Bunker"/>.
        /// </summary>
        /// <param name="x">Horizontal position of the bunker.</param>
        /// <param name="y">Vertical position of the bunker.</param>
        public Bunker(int x, int y)
        {
            X = x;
            Y = y;
            Sprite = SPRITE;
            Lives = LIVES;
            Side = Side.NEUTRAL;
        }

        /// <summary>
        /// Draws the bunker on the screen.
        /// </summary>
        /// <param name="forced">Whether or not the bunker has to be forcefully
        /// drawn on the screen.</param>
        /// <remarks>
        /// A bunker is gray.
        /// </remarks>
        public override void Draw(bool forced)
        {
            Helper.Erase(OldX, OldY, Sprite.Length);
            Console.SetCursorPosition(X, Y);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(Sprite);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Checks if the bunker is alive.
        /// </summary>
        /// <returns>True if the bunker is alive, false otherwise.</returns>
        public override bool IsAlive()
        {
            return Lives > 0;
        }

        /// <summary>
        /// Updates the bunker's state.
        /// </summary>
        /// <remarks>
        /// If the bunker has been damaged, its sprite changes.
        /// </remarks>
        public override void Update()
        {
            if (Lives < LIVES)
                Sprite = SPRITE_DAMAGED;
        }

        /// <summary>
        /// Inflicts damages to the bunker.
        /// </summary>
        public void LoseHP()
        {
            --Lives;
        }

        /// <inheritdoc/>
        public override void CollideWith(Missile m)
        {
            LoseHP();
            m.LoseHP();
        }
    }
}
