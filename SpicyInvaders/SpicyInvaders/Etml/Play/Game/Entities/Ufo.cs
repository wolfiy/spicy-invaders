﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 19th, 2024

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents an UFO type enemy. An UFO is harder to destroy as it has 
    /// multiple lives and its shoot rate is fairly high.
    /// </summary>
    internal class Ufo : Enemy
    {
        /// <summary>
        /// Console representation of an UFO.
        /// </summary>
        public const string SPRITE = "((-))";

        /// <summary>
        /// Number of lives of an UFO.
        /// </summary>
        private const int LIVES = 2;

        /// <summary>
        /// Shootrate of an UFO.
        /// </summary>
        private const int SHOOTRATE = 30;

        /// <summary>
        /// Console representation of a damaged UFO.
        /// </summary>
        private const string DAMAGED_SPRITE = "{(~)}";

        /// <summary>
        /// Initializes a new <see cref="Ufo"/>. The ufo has two lives.
        /// </summary>
        /// <param name="x">Horizontal position of the ufo.</param>
        /// <param name="y">Vertical position of the ufo.</param>
        public Ufo(int x, int y) : base(x, y) 
        { 
            Shootrate = SHOOTRATE;
            Lives = LIVES;
            Sprite = SPRITE;
        }

        /// <summary>
        /// Updates the ufo's state. If it has been hit, its sprite changes.
        /// </summary>
        public override void Update()
        {
            base.Update();

            // Change sprite if damaged
            if (Lives < LIVES)
                Sprite = DAMAGED_SPRITE;
        }
    }
}
