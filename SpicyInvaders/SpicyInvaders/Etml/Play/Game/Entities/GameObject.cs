﻿/// ETML
/// Author: Sebastien TILLE
/// Date: January 18th, 2023

using Ch.Etml.Play.Game.Engine;

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Refers to the alignment of a <see cref="GameObject"/>.
    /// </summary>
    public enum Side
    {
        /// <summary>
        /// Represents an object which is friendly toward the player.
        /// </summary>
        PLAYER,

        /// <summary>
        /// Represents an object which is hostile toward the player.
        /// </summary>
        INVADER,

        /// <summary>
        /// Represents a neutral object which is neither friendly nor hostile
        /// toward the player.
        /// </summary>
        NEUTRAL
    }

    /// <summary>
    /// Represents an abstract, in-game object.
    /// </summary>
    public abstract class GameObject : IUpdatable
    {
        /// <summary>
        /// Horizontal position, from left to right.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Vertical position, from top to bottom.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Alignment of the <see cref="GameObject"/>.
        /// </summary>
        public Side Side { get; protected set; }

        /// <summary>
        /// In-game representation of the object.
        /// </summary>
        public string Sprite { get; set; }

        /// <summary>
        /// Number of lives of the object.
        /// </summary>
        public int Lives { get; set; }

        /// <summary>
        /// Horizontal position from left to right during the previous game 
        /// loop.
        /// </summary>
        protected int OldX { get; set; }

        /// <summary>
        /// Vertical position from top to bottom during the previous game 
        /// loop.
        /// </summary>
        protected int OldY { get; set; }

        /// <summary>
        /// Draws the game object to the screen.
        /// </summary>
        /// <param name="isForced">True if the object has to be drawn 
        /// regardless of any other conditions.</param>
        /// <remarks>
        /// Upon game unpause, isForce shall be set to true to make sure 
        /// every object is properly drawn even if their positions did not
        /// change in the meantime.
        /// </remarks>
        public abstract void Draw(bool isForced);

        /// <summary>
        /// Checks if the game object is alive.
        /// </summary>
        /// <returns>True if the entity is alive, false otherwise.</returns>
        public abstract bool IsAlive();

        /// <summary>
        /// Updates the game object's state.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Handles collisions with <see cref="Missile"/> objects.
        /// </summary>
        /// <param name="m">A missile.</param>
        public abstract void CollideWith(Missile m);

        #region Getters / setters
        /// <summary>
        /// Gets the length of the sprite.
        /// </summary>
        /// <returns>The length of the sprite.</returns>
        public int GetSpriteSize()
        {
            return Sprite.Length;
        }
        #endregion
    }
}
