﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 16th, 2024

using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using static Ch.Etml.Play.Game.Engine.Constants;

[assembly: InternalsVisibleTo("SpicyInvaders.Tests")]

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents the different types of enemies.
    /// </summary>
    public enum EnemyType
    {
        /// <summary>
        /// Represents an enemy of type <see cref="Alien"/>.
        /// </summary>
        ALIEN,

        /// <summary>
        /// Represents an enemy of type <see cref="Squid"/>.
        /// </summary>
        SQUID,

        /// <summary>
        /// Represents an enemy of type <see cref="Ufo"/>.
        /// </summary>
        UFO
    }

    /// <summary>
    /// Represents a container used to hold multiple instances of the 
    /// <see cref="Enemy"/> class.
    /// </summary>
    public class EnemyContainer : GameObject
    {
        /// <summary>
        /// Default number of rows.
        /// </summary>
        private const int NB_ROWS = 3;

        /// <summary>
        /// Shift used to add a new row of enemies.
        /// </summary>
        private const int SIZE_SHIFT = 1;

        /// <summary>
        /// Initial width of the container.
        /// </summary>
        private readonly int _baseWidth;

        /// <summary>
        /// Direction of the container.
        /// </summary>
        public bool IsRight { get; private set; }

        /// <summary>
        /// The set of all enemies.
        /// </summary>
        public HashSet<Enemy> Enemies { get; private set; }

        /// <summary>
        /// Sound controller.
        /// </summary>
        public GameController Controller { get; set; }

        /// <summary>
        /// Whether the block is moving downwards.
        /// </summary>
        private bool _down;

        /// <summary>
        /// Is true if at least one of the remaining enemies has
        /// reached the <see cref="Constants.ENEMY_THRESHOLD"/>.
        /// </summary>
        private bool _atBottom;

        /// <summary>
        /// Whether or not the <see cref="EnemyContainer"/> should move on the
        /// current game loop.
        /// </summary>
        private bool _move;

        /// <summary>
        /// Initial left bound.
        /// </summary>
        private int _leftLimit = 0;

        /// <summary>
        /// Initial right bound.
        /// </summary>
        private int _rightLimit = 0;

        /// <summary>
        /// <see cref="GameView"/> instance which the <see cref="EnemyContainer"/>
        /// belongs to.
        /// </summary>
        private GameModel _model;

        /// <summary>
        /// Pseudo-random number generator used to generate attacks.
        /// </summary>
        private Random _rdm;

        /// <summary>
        /// Size of the container.
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Initializes a new <see cref="EnemyContainer"/>.
        /// </summary>
        public EnemyContainer(int x, int y, int baseWidth, GameModel model)
        {
            if (baseWidth < 4)
                throw new ArgumentException(
                    "Base width should at least be able to accomodate one enemy!"
                );

            X = x;
            Y = y;
            _baseWidth = baseWidth;
            _model = model;
            _rdm = new Random();
            _move = true;
            _down = false;
            _atBottom = false;
            Size = new Size(baseWidth, 0);
            Enemies = new HashSet<Enemy>();
            IsRight = false;

            // Fill the container with enemies
            AddLine(EnemyType.UFO);
            AddLine(EnemyType.ALIEN);
            AddLine(EnemyType.SQUID);
        }

        #region Container filling
        /// <summary>
        /// Adds a new line of enemies to the enemy container.
        /// </summary>
        public void AddLine(EnemyType type)
        {
            // Finding how many enemies can fit
            int enemySpriteLength = GetDefaultEnemySpriteSize(type);
            int nbEnemies = _baseWidth / enemySpriteLength;

            // Fill the row
            for (int i = 0; i < nbEnemies; ++i)
            {
                switch (type)
                {
                    case EnemyType.ALIEN:
                        Enemies.Add(
                            new Alien(X + i * (enemySpriteLength + ENEMY_SPACING),
                                      Y * (Size.Height + ENEMY_SPACING))
                        );
                        break;
                    case EnemyType.SQUID:
                        Enemies.Add(
                            new Squid(X + i * (enemySpriteLength + ENEMY_SPACING),
                                      Y * (Size.Height + ENEMY_SPACING))
                        );
                        break;
                    case EnemyType.UFO:
                        Enemies.Add(
                            new Ufo(X + i * (enemySpriteLength + ENEMY_SPACING),
                                    Y * (Size.Height + ENEMY_SPACING))
                        );
                        break;
                }
            }

            UpdateSize();
        }

        /// <summary>
        /// Gets the length of the default console representation of an enemy.
        /// </summary>
        /// <param name="type">The type of enemy from which to get the length.
        /// </param>
        /// <returns>The length of the enemy's default sprite.</returns>
        /// <exception cref="ArgumentException">If the specified type does not
        /// exist.</exception>
        private int GetDefaultEnemySpriteSize(EnemyType type)
        {
            switch (type)
            {
                case EnemyType.ALIEN:
                    return Alien.SPRITE.Length;
                case EnemyType.SQUID:
                    return Squid.SPRITE.Length;
                case EnemyType.UFO:
                    return Ufo.SPRITE.Length;
                default:
                    throw new ArgumentException(
                        $"Type {type} is not a valid enemy type."
                    );
            }
        }
        #endregion

        /// <summary>
        /// Updates the container's size.
        /// </summary>
        public void UpdateSize()
        {
            int minX = Enemies.Min(e => e.X);
            int minY = Enemies.Min(e => e.Y);
            int maxX = Enemies.Max(e => e.X);
            int maxY = Enemies.Max(e => e.Y);
            int offset = Enemies.Max(e => e.GetSpriteSize());

            int width = maxX - minX + offset;
            int height = maxY - minY;

            Size = new Size(width + SIZE_SHIFT, 
                            height + SIZE_SHIFT);
        }

        /// <summary>
        /// Draws the enemy container on the screen.
        /// </summary>
        /// <param name="isForced">Whether to force the container's redraw.
        /// </param>
        public override void Draw(bool isForced)
        {
            foreach (var enemy in Enemies)
               enemy.Draw(isForced);
        }

        /// <summary>
        /// Checks if the container is alive. A container is alive if at
        /// least one of the enemies in its <see cref="Enemies"/> hashset is
        /// alive.
        /// </summary>
        /// <returns>True if the container is alive, false otherwise.</returns>
        public override bool IsAlive()
        {
            return Enemies.Any(e => e.IsAlive());
        }

        /// <summary>
        /// Updates the container's state and handles movements.
        /// </summary>
        public override void Update()
        {
            // Move once every two game loops
            _move = !_move;

            // Container movement
            if (IsAlive() && _move)
            {
                // The block cannot go further than this
                _rightLimit = Console.WindowWidth - Size.Width;
                OldX = X;
                OldY = Y;

                MoveContainer();
                MoveEnemies();
                UpdateSize();
                _down = false;
            }

            if (!IsAlive())
            {
                // Ensure everything has been cleared
                for (int i = 0; i < NB_ROWS; ++i)
                    Helper.Erase(X, Y + i, _baseWidth);

                _model.GameObjects.Remove(this);

                return;
            }

            UpdateSize();
        }

        /// <summary>
        /// Moves the container itself.
        /// </summary>
        private void MoveContainer()
        {
            // End game if enemies reached the bottom
            if (_model.Difficulty != Difficulty.EASY &&
                _atBottom)
                Controller.SetGameState(State.LOSE);

            // Move the container
            if (X >= _leftLimit && IsRight)
                X = Math2.Clamp(_leftLimit, _rightLimit, ++X);
            if (X <= _rightLimit && !IsRight)
                X = Math2.Clamp(_leftLimit, _rightLimit, --X);

            // Go down if on a side
            if (!_atBottom && (X == 0 || X == _rightLimit))
            {
                _down = true;

                // If bottom has been reached
                if (Y + Size.Height >= Constants.ENEMY_THRESHOLD)
                    _atBottom = true;
            }

            // Change direction if on a side
            if (X <= 0)
                IsRight = true;
            else if (X >= _rightLimit)
                IsRight = false;

            // Go down if possible
            if (_down && !_atBottom)
                Y = Math2.Clamp(0, Constants.ENEMY_THRESHOLD, ++Y);
        }

        /// <summary>
        /// Moves the enemies contained in the container.
        /// </summary>
        private void MoveEnemies()
        {
            // Position difference
            int shiftX = X - OldX;
            int shiftY = Y - OldY;

            // Move enemies
            foreach (Enemy e in Enemies.ToList())
            {
                int enemySpriteLength = e.GetSpriteSize();

                // Erase old enemy position
                Helper.Erase(e.X, e.Y, enemySpriteLength);

                // New enemy position
                e.X = Math2.Clamp(0, Console.WindowWidth - enemySpriteLength,
                                  e.X + shiftX);
                e.Y = Math2.Clamp(0, Constants.ENEMY_THRESHOLD,
                                  e.Y + shiftY);

                if (!e.IsAlive())
                {
                    Helper.Erase(e.X, e.Y, e.Sprite.Length);
                    Enemies.Remove(e);
                    UpdateSize();
                    continue;
                }
                else
                {
                    // Shoot. Only the bottom row can shoot on easy mode.
                    if (_rdm.Next(e.Shootrate) == 0
                        && (e.Y >= Y + Size.Height - SIZE_SHIFT
                            || _model.Difficulty == Difficulty.HARD))
                    {
                        _model.GameObjects.Add(
                            new Missile(e.X + enemySpriteLength / 2, e.Y, 
                                        _model, Side.INVADER)
                        );

                        if (Controller.GetSoundSetting())
                            Controller.PlaySound(Sound.SHOOT);
                    }

                    /* 
                     * Note: drawing enemies here performs better when a
                     * lot is happening on the screen. However, drawing
                     * inside of the update method does not make any snese.
                     */
                    e.Update();
                }
            }
        }

        /// <inheritdoc/>
        public override void CollideWith(Missile m)
        {
            // The container cannot be directly interacted with.
        }
    }
}
