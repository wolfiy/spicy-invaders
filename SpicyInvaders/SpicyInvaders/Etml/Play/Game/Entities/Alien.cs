﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 18th, 2024

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents an alien enemy. An alien has only one life but shoots faster
    /// than a squid.
    /// </summary>
    internal class Alien : Enemy
    {
        /// <summary>
        /// Console representation of an alien.
        /// </summary>
        public const string SPRITE = "(~~~)";

        /// <summary>
        /// Number of lives of an alien.
        /// </summary>
        private const int LIVES = 1;

        /// <summary>
        /// Shootrate of an alien.
        /// </summary>
        private const int SHOOTRATE = 42;

        /// <summary>
        /// Initializes a new <see cref="Alien"/>.
        /// </summary>
        /// <param name="x">Horizontal position of the alien.</param>
        /// <param name="y">Vertical position of the alien.</param>
        public Alien(int x, int y) : base(x, y) 
        { 
            Shootrate = SHOOTRATE;
            Sprite = SPRITE;
            Lives = LIVES;
        }
    }
}
