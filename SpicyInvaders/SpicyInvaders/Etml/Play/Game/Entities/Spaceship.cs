﻿/// ETML
/// Author: Sebastien TILLE
/// Date: January 18th, 2023

using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Models;
using System;
using System.Timers;

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents an in-game spaceship. The spaceship is controlled by the 
    /// player.
    /// </summary>
    public class Spaceship : GameObject, IDestructible
    {
        /// <summary>
        /// Duration of the blinking animation in ms.
        /// </summary>
        private const int BLINK_DURATION = 200;

        /// <summary>
        /// Game model which the spaceship belongs to.
        /// </summary>
        private readonly GameModel _model;

        /// <summary>
        /// Timer used to blink the spaceship.
        /// </summary>
        private readonly Timer _blinkTimer;

        /// <summary>
        /// Timer for the duration of the blinking animation.
        /// </summary>
        private readonly Timer _hitTimer;

        /// <summary>
        /// Duration of the hit animation / invincibility in ms.
        /// </summary>
        private const int HIT_DURATION = 3000;

        /// <summary>
        /// Whether the spaceship can get hit.
        /// </summary>
        public bool IsInvincible { get; private set; }

        /// <summary>
        /// Whether the spaceship is blinking.
        /// </summary>
        private bool _isBlinking;

        /// <summary>
        /// Whether the spaceship is visible.
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// Previous lives of the spaceship.
        /// </summary>
        private int _oldLives;

        /// <summary>
        /// Initializes a new <see cref="Spaceship"/>.
        /// </summary>
        /// <param name="lives">Number of lives available.</param>
        /// <param name="x">Horizontal position of the spaceship.</param>
        /// <param name="y">Vertical position of the spaceship.</param>
        public Spaceship(int lives, int x, int y, GameModel model)
        {
            Sprite = Constants.SPRITE_SPACESHIP;
            Lives = _oldLives = lives;
            Side = Side.PLAYER;
            X = x;
            Y = y;
            _model = model;

            IsInvincible = false;
            _isBlinking = false;
            _isVisible = true;
            _blinkTimer = new Timer(BLINK_DURATION);
            _hitTimer = new Timer(HIT_DURATION);
            _blinkTimer.Elapsed += Timer_Elapsed;
            _hitTimer.Elapsed += HitTimer_Elapsed;
            _blinkTimer.Start();
        }

        /// <summary>
        /// Fires a projectile. The projectile is an instance of the 
        /// <see cref="Missile"/> class.
        /// </summary>
        public void Shoot()
        {
            _model.GameObjects.Add(
                new Missile(X + GetSpriteSize() / 2,
                            Y - Constants.SPACER,
                            _model,
                            Side.PLAYER)
            );

            if (_model.IsSoundOn)
                _model.SoundController.PlaySound(Sound.SHOOT);
        }

        /// <summary>
        /// Moves the spaceship horizontally.
        /// </summary>
        /// <param name="isRight">True if moving the spaceship to the right, 
        /// false if moving to the left.</param>
        public void Move(bool isRight)
        {
            OldX = X;
            _ = isRight ? ++X : --X;
        }

        /// <summary>
        /// Draws the spaceship. The spaceship will be redrawn only if its
        /// position has changed since the last time it was drawn.
        /// </summary>
        /// <remarks>
        /// A recently 
        /// hit spaceship has a blinking effect.
        /// </remarks>
        /// <param name="isForced">Whether the spaceship should be redrawn 
        /// even if its position or state has not changed.</param>
        public override void Draw(bool isForced)
        {
            if (((OldX != X || OldY != Y) && !_isBlinking)
                || (_isVisible && _isBlinking)
                || isForced)
            {
                Helper.Erase(OldX, Y, Sprite.Length);
                Console.SetCursorPosition(X, Y);

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write(Sprite);
                Console.ForegroundColor = ConsoleColor.White;

                OldX = X;
                OldY = Y;
            }
            else if (!_isVisible && _isBlinking)
            {
                Helper.Erase(OldX, OldY, Sprite.Length);
            }
        }

        /// <summary>
        /// Checks if the spaceship is alive.
        /// </summary>
        /// <returns>True if the spaceship is alive, false otherwise.</returns>
        public override bool IsAlive()
        {
            return Lives > 0;
        }

        /// <summary>
        /// Updates the spaceship's state.
        /// </summary>
        /// <remarks>
        /// If the spaceship has been hit, the blinking animation is initiated
        /// alongside a short invicibility time periode.
        /// </remarks>
        public override void Update()
        {
            if (_oldLives != Lives)
            {
                _hitTimer.Start();
                _isBlinking = true;
                _oldLives = Lives;

                if (_model.Difficulty is Difficulty.EASY)
                    IsInvincible = true;
            }
        }

        /// <inheritdoc/>
        public override void CollideWith(Missile m)
        {
            LoseHP();
            m.LoseHP();
        }

        /// <summary>
        /// Inflicts damages to the spaceship if it is vulnerable.
        /// </summary>
        public void LoseHP()
        {
            if (!IsInvincible)
                --Lives;
        }

        #region Events
        /// <summary>
        /// Toggles the visibility of the spaceship used for the blinking 
        /// animation.
        /// </summary>
        /// <param name="sender">The blink timer.</param>
        /// <param name="e">A timer elapsed event.</param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _isVisible = !_isVisible;
        }

        /// <summary>
        /// Stops the blinking animation.
        /// </summary>
        /// <param name="sender">The hit timer.</param>
        /// <param name="e">A timer elapsed event.</param>
        private void HitTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Timer t = (Timer)sender;
            IsInvincible = false;
            _isBlinking = false;
            _isVisible = true;
            t.Stop();

            // Will force a redraw
            OldX = 0;
        }
        #endregion
    }
}
