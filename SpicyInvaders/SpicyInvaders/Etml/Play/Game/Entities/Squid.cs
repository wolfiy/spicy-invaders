﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 18th, 2024

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents a squid type enemy. A squid shoots at a medium pace and has 
    /// one life.
    /// </summary>
    internal class Squid : Enemy
    {
        /// <summary>
        /// Console representation of a squid enemy.
        /// </summary>
        public const string SPRITE = "v^v";

        /// <summary>
        /// Number of lives of a squid.
        /// </summary>
        private const int LIVES = 1;

        /// <summary>
        /// Shootrate of a squid.
        /// </summary>
        private const int SHOOTRATE = 50;

        /// <summary>
        /// Initializes a new <see cref="Squid"/>.
        /// </summary>
        /// <param name="x">Horizontal position of the squid.</param>
        /// <param name="y">Vertical position of the squid.</param>
        public Squid(int x, int y) : base(x, y)
        {
            Shootrate = SHOOTRATE;
            Sprite = SPRITE;
            Lives = LIVES;
        }
    }
}
