﻿/// ETML
/// Autho: Sebastien TILLE
/// Date: February 23rd, 2024

using System;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Models;

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represents an in game missile.
    /// </summary>
    public class Missile : GameObject, IDestructible
    {
        /// <summary>
        /// Number of lives of a missile.
        /// </summary>
        private const int LIVES = 1;

        /// <summary>
        /// Console representation of an invader missile.
        /// </summary>
        private const string SPRITE_INVADER = "i";

        /// <summary>
        /// Console representation of a player missile.
        /// </summary>
        private const string SPRITE_PLAYER = "!";

        /// <summary>
        /// Game model from which the missile was shot.
        /// </summary>
        private readonly GameModel _model;

        /// <summary>
        /// Initializes a new <see cref="Missile"/>.
        /// </summary>
        /// <param name="x">Horizontal position of the missile.</param>
        /// <param name="y">Vertical position of the missile.</param>
        public Missile(int x, int y, GameModel game, Side side)
        {
            X = x;
            Y = y;
            Lives = LIVES;
            Side = side;
            Sprite = Side == Side.PLAYER ? SPRITE_PLAYER 
                                         : SPRITE_INVADER;
            _model = game;
        }

        /// <summary>
        /// Draws the missile on the screen.
        /// </summary>
        /// <param name="forced">Whether the missile should be redrawn 
        /// regardless of its previous position.</param>
        public override void Draw(bool forced)
        {
            if (OldX != X || OldY != Y || forced)
            {
                Helper.Erase(OldX, OldY, Sprite.Length);
                Console.SetCursorPosition(X, Y);
                Console.Write(Sprite);

                OldX = X;
                OldY = Y;
            }
        }

        /// <summary>
        /// Checks if the missile is alive.
        /// </summary>
        /// <returns></returns>
        public override bool IsAlive()
        {
            return Lives > 0;
        }

        /// <summary>
        /// Updates the misile's state and checks for collisions with other game
        /// objects.
        /// </summary>
        /// <remarks>
        /// If the missile is on the player's side, it will move up. If it is on
        /// the invaders' side, it moves down.
        /// </remarks>
        public override void Update()
        {
            // Keep track of old position
            OldX = X;
            OldY = Y;

            // Move missile
            if (Y > 0 && Y < Console.WindowHeight && IsAlive())
                OldY = Side == Side.PLAYER ? Y-- : Y++;
            if (Y == 0 || Y == Console.WindowHeight - 2)
                LoseHP();

            // Collisions with game objects besides enemies
            foreach (GameObject obj in _model.GameObjects)
            {
                // Check for collisions
                if (Helper.CheckCollisions(obj, this) && IsAlive())
                {
                    // Prevent friendly fire
                    if (this.Side != obj.Side)
                    {
                        if (this.Side == Side.INVADER &&
                            obj.Side == Side.PLAYER &&
                            _model.IsSoundOn)
                            _model.SoundController.PlaySound(Sound.PLAYER_HIT);
                        obj.CollideWith(this);
                        break;
                    }
                }
            }

            // Collisions with enemies
            foreach (Enemy e in _model.EnemyContainer.Enemies)
            {
                if (this.Side != Side.INVADER && 
                    Helper.CheckCollisions(e, this) &&
                    IsAlive())
                {
                    if (_model.IsSoundOn)
                        _model.SoundController.PlaySound(Sound.EXPLOSION);

                    e.CollideWith(this);
                    _model.Score.Add(Constants.SCORE_ENEMY);
                    Helper.Erase(e.X, e.Y, e.Sprite.Length);
                    break;
                }
            }

            // Erase sprite if destroyed
            if (!IsAlive())
                Helper.Erase(X, Y, Sprite.Length);
        }

        /// <summary>
        /// Inflict damages to the missile.
        /// </summary>
        public void LoseHP()
        {
            Lives = 0;
        }

        /// <inheritdoc/>
        public override void CollideWith(Missile m)
        {
            m.LoseHP();
            this.LoseHP();
        }
    }
}
