﻿/// ETML
/// Author: Sebastien TILLE
/// Date; April 19th, 2024

using Ch.Etml.Play.Game.Engine;
using System;

namespace Ch.Etml.Play.Game.Entities
{
    /// <summary>
    /// Represent an in game enemy. An enemy can attack the player.
    /// </summary>
    /// <remarks>
    /// En enemy is managed by an instance of the 
    /// <see cref="EnemyContainer"/> class.
    /// </remarks>
    public class Enemy : GameObject, IDestructible
    {
        /// <summary>
        /// Default shootrate for all enemies.
        /// </summary>
        private const int DEFAULT_SHOOTRATE = 70;

        /// <summary>
        /// Rate of fire of the enemy. The lower this number is, the faster
        /// the enemy will shoot.
        /// </summary>
        public int Shootrate { get; protected set; }

        /// <summary>
        /// Initializes a new <see cref="Enemy"/>.
        /// </summary>
        public Enemy(int x, int y)
        {
            X = x;
            Y = y;
            Side = Side.INVADER;
            Shootrate = DEFAULT_SHOOTRATE;
        }

        /// <summary>
        /// Draws the enemy.
        /// </summary>
        /// <remarks>
        /// Enemies are drawn in yellow.
        /// </remarks>
        /// <param name="isForced">Whether the enemy should be forcefully drawn.
        /// </param>
        public override void Draw(bool isForced)
        {
            Console.SetCursorPosition(X, Y);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(Sprite);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Checks if the enemy is alive.
        /// </summary>
        /// <returns>True if the enemy is alive, false otherwise.</returns>
        public override bool IsAlive()
        {
            return Lives > 0;
        }

        /// <summary>
        /// Inflicts damages to the enemy.
        /// </summary>
        public void LoseHP()
        {
            --Lives;
        }

        /// <summary>
        /// Updates the enemy's state.
        /// </summary>
        public override void Update()
        {
            // Avoid blocking projectile before the enemy is properly removed
            // from the game.
            if (!IsAlive())
                X = Y = 0;
        }

        /// <inheritdoc/>
        public override void CollideWith(Missile m)
        {
            LoseHP();
            m.LoseHP();
        }
    }
}
