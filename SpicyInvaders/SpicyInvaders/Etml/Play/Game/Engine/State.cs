﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 3rd, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// States the game can be in.
    /// </summary>
    public enum State
    {
        /// <summary>
        /// Game is running.
        /// </summary>
        RUNNING,

        /// <summary>
        /// Game is paused.
        /// </summary>
        PAUSED,

        /// <summary>
        /// Game has ended with a win.
        /// </summary>
        WIN,

        /// <summary>
        /// Game has ended with a loss.
        /// </summary>
        LOSE
    }
}