﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 25th, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Represents the types of menus available in the game.
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// The main menu of the game.
        /// </summary>
        MAIN,

        /// <summary>
        /// The pause menu, displayed when the game is paused.
        /// </summary>
        PAUSE,

        /// <summary>
        /// The settings menu, used to adjust game options.
        /// </summary>
        SETTINGS
    }
}
