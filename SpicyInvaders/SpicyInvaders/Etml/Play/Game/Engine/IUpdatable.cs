﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 22nd, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Represents an element that can be updated through time.
    /// </summary>
    internal interface IUpdatable
    {
        /// <summary>
        /// Updates the element. Typicall, this is called once per frame.
        /// </summary>
        void Update();
    }
}
