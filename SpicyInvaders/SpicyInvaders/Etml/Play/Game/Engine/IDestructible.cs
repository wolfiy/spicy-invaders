﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 22nd, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Represents an element that can take damages and be destroyed.
    /// </summary>
    interface IDestructible
    {
        /// <summary>
        /// Inflicts damages to the element.
        /// </summary>
        void LoseHP();
    }
}
