﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 18th, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Class used to store game constants. Large texts have been generated
    /// using the "Slant" font on the following website:
    /// https://patorjk.com/software/taag/#p=display&f=Slant&t=
    /// </summary>
    internal class Constants
    {
        #region Game
        /// <summary>
        /// Margin between the game and window borders.
        /// </summary>
        public const int MARGIN = 2;

        /// <summary>
        /// Console height.
        /// </summary>
        public const int WINDOW_HEIGHT = 30;

        /// <summary>
        /// Console width.
        /// </summary>
        public const int WINDOW_WIDTH = 81;

        /// <summary>
        /// Width of menu items.
        /// </summary>
        public const int BUTTON_WIDTH = 24;

        /// <summary>
        /// Lowest point an enemy can reach.
        /// </summary>
        public const int ENEMY_THRESHOLD = WINDOW_HEIGHT - 10;

        /// <summary>
        /// Leftmost position the player can attain.
        /// </summary>
        public const int PLAYABLE_BOUND_LEFT = 1;

        /// <summary>
        /// Rightmost position the player can attain.
        /// </summary>
        public const int PLAYABLE_BOUND_RIGHT = WINDOW_WIDTH - 1;

        /// <summary>
        /// Single-cell spacer.
        /// </summary>
        public const int SPACER = 1;
        #endregion

        #region Sprites
        /// <summary>
        /// In-game representation of a <see cref="Spaceship"/>.
        /// </summary>
        public const string SPRITE_SPACESHIP = "-^-";
        #endregion

        #region Entites
        /// <summary>
        /// Default lives of a <see cref="Entities.Spaceship"/>
        /// </summary>
        public const int LIVES_SPACESHIP = 3;

        /// <summary>
        /// Space between two enemies in an <see cref="EnemyContainer"/>.
        /// </summary>
        public const int ENEMY_SPACING = 1;

        /// <summary>
        /// Player position at the beginning of a game.
        /// </summary>
        public static readonly (int x, int y) INITIAL_PLAYER_POSITION =
            (WINDOW_WIDTH / 2 - SPRITE_SPACESHIP.Length / 2, WINDOW_HEIGHT - 3);

        /// <summary>
        /// Default enemy spawn position.
        /// </summary>
        public static readonly (int x, int y) ENEMY_SPAWN_POSITION =
            (1, 1);

        /// <summary>
        /// Default enemy block width.
        /// </summary>
        public const int DEFAULT_ENEMY_WIDTH = 20;
        #endregion

        #region Bunkers
        public const int BUNKER_WIDTH = 19;
        public const int BUNKER_HEIGHT = 2;

        /// <summary>
        /// Default vertical position of a <see cref="Bunker"/>.
        /// </summary>
        public const int BUNKER_POSITION_Y = WINDOW_HEIGHT - 7;

        /// <summary>
        /// Default bunker positions.
        /// </summary>
        public static readonly (int x, int y)[] DEFAULT_BUNKER_POSITIONS = new[]
        {
            ( 3, BUNKER_POSITION_Y ),
            ( WINDOW_WIDTH / 2 - BUNKER_WIDTH / 2, BUNKER_POSITION_Y ),
            ( WINDOW_WIDTH - BUNKER_WIDTH - 2, BUNKER_POSITION_Y )
        };
        #endregion

        #region Scores
        /// <summary>
        /// Score awarded for killing an <see cref="Enemy"/>.
        /// </summary>
        public const int SCORE_ENEMY = 10;
        #endregion

        #region Descriptions
        /// <summary>
        /// Navigation help.
        /// </summary>
        public const string MAIN_DESCRIPTION =
            "Navigate using the arrow keys. Select with space.";

        /// <summary>
        /// Quit help.
        /// </summary>
        public const string HIGH_SCORES_DESCRIPTION =
            "Press <enter> to go back.";
        #endregion

        #region Titles
        /// <summary>
        /// Main title.
        /// </summary>
        public const string MAIN_TITLE =
@"   _____       _               ____                     __              
  / ___/____  (_)______  __   /  _/___ _   ______ _____/ /__  __________
  \__ \/ __ \/ / ___/ / / /   / // __ \ | / / __ `/ __  / _ \/ ___/ ___/
 ___/ / /_/ / / /__/ /_/ /  _/ // / / / |/ / /_/ / /_/ /  __/ /  (__  ) 
/____/ .___/_/\___/\__, /  /___/_/ /_/|___/\__,_/\__,_/\___/_/  /____/  
    /_/           /____/                                                ";

        /// <summary>
        /// Pause title.
        /// </summary>
        public const string PAUSE_TITLE =
@"    ____                      
   / __ \____ ___  __________ 
  / /_/ / __ `/ / / / ___/ _ \
 / ____/ /_/ / /_/ (__  )  __/
/_/    \__,_/\__,_/____/\___/ 
                              ";

        /// <summary>
        /// Game over title.
        /// </summary>
        public const string GAME_OVER =
@"   ______                                             __
  / ____/___ _____ ___  ___     ____ _   _____  _____/ /
 / / __/ __ `/ __ `__ \/ _ \   / __ \ | / / _ \/ ___/ / 
/ /_/ / /_/ / / / / / /  __/  / /_/ / |/ /  __/ /  /_/  
\____/\__,_/_/ /_/ /_/\___/   \____/|___/\___/_/  (_)   
                                                        ";

        /// <summary>
        /// High scores title.
        /// </summary>
        public const string TITLE_SCORES =
@"    __  ___       __                                     
   / / / (_)___ _/ /_     ______________  ________  _____
  / /_/ / / __ `/ __ \   / ___/ ___/ __ \/ ___/ _ \/ ___/
 / __  / / /_/ / / / /  (__  ) /__/ /_/ / /  /  __(__  ) 
/_/ /_/_/\__, /_/ /_/  /____/\___/\____/_/   \___/____/  
        /____/                                           
";

        /// <summary>
        /// Settings title.
        /// </summary>
        public const string SETTINGS_MENU =
@"   _____      __  __  _                 
  / ___/___  / /_/ /_(_)___  ____ ______
  \__ \/ _ \/ __/ __/ / __ \/ __ `/ ___/
 ___/ /  __/ /_/ /_/ / / / / /_/ (__  ) 
/____/\___/\__/\__/_/_/ /_/\__, /____/  
                          /____/        ";

        /// <summary>
        /// About title.
        /// </summary>
        public const string ABOUT_TITLE =
@"    ___    __                __ 
   /   |  / /_  ____  __  __/ /_
  / /| | / __ \/ __ \/ / / / __/
 / ___ |/ /_/ / /_/ / /_/ / /_  
/_/  |_/_.___/\____/\__,_/\__/  
                                ";
        #endregion
    }
}
