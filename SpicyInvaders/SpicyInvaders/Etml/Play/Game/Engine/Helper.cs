﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 18th, 2024

using Ch.Etml.Play.Game.Entities;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SpicyInvaders.Tests")]

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Helper class used for various operations such as erasing sprites or
    /// checking collisions.
    /// </summary>
    internal class Helper
    {
        /// <summary>
        /// An empty "pixel".
        /// </summary>
        private const string EMPTY = " ";

        /// <summary>
        /// Checks if a collision with a <see cref="Missile"/> can happen.
        /// </summary>
        /// <param name="target">The object to collide with.</param>
        /// <param name="missile">The missile.</param>
        /// <returns>True if a collision can happen, false otherwise.</returns>
        public static bool CheckCollisions(GameObject target, Missile missile)
        {
            // A collision can only happen if both the target and the missile 
            // have the same height position
            if (target.Y != missile.Y)
                return false;

            for (int i = 0; i < target.Sprite.Length; ++i)
            {
                if (missile.X == target.X + i)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Clears a specified area on the console.
        /// </summary>
        /// <param name="x">Horizontal position of the leftmost side of the area
        /// to clear.</param>
        /// <param name="y">Vertical position of the leftmost side of the area
        /// to clear.</param>
        /// <param name="length">Length of the area to clear.</param>
        public static void Erase(int x, int y, int length)
        {
            Console.SetCursorPosition(x, y);
            for (int i = 0; i < length; ++i)
                Console.Write(EMPTY);
        }
    }
}
