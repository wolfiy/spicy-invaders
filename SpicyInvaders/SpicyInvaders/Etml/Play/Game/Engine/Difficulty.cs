﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 3rd, 2024

namespace Ch.Etml.Play.Game.Engine
{
    /// <summary>
    /// Represents the different difficulty levels of the game.
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// Easy difficulty level.
        /// </summary>
        EASY = 0,

        /// <summary>
        /// Medium difficulty level.
        /// </summary>
        MEDIUM = 1,

        /// <summary>
        /// Hard difficulty level.
        /// </summary>
        HARD = 2
    }
}