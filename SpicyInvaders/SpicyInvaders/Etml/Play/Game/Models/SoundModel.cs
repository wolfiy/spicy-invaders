﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 19th, 2024

using NAudio.Wave;

namespace Ch.Etml.Play.Game.Models
{
    /// <summary>
    /// Represents the different types of sounds that can be played throught
    /// the game.
    /// </summary>
    public enum Sound
    {
        /// <summary>
        /// Explosion sound effect played when missiles collide.
        /// </summary>
        EXPLOSION,

        /// <summary>
        /// Shooting sound effect used when shooting a missile.
        /// </summary>
        SHOOT,

        /// <summary>
        /// Sound effect played when the player is hit.
        /// </summary>
        PLAYER_HIT,

        /// <summary>
        /// Sound effect used throughout menus.
        /// </summary>
        SELECTION,

        /// <summary>
        /// Main menu background music.
        /// </summary>
        MENU,

        /// <summary>
        /// Gameplay background music.
        /// </summary>
        GAMEPLAY
    }

    /// <summary>
    /// Holds sound-related components of the game.
    /// </summary>
    public class SoundModel
    {
        /// <summary>
        /// Explosion sound effect.
        /// https://sfxr.me/#7BMHBGTvdKADvzBApi1RbXNTgttYsjTXwsWfUQNmcDS8dG8XNDVWxct4BNwmA2zaC2TGhrsYb5DAbRusfgbw7jygyRtgokdbAa5ymVjPdZ1X8dBQ7qD7QJdPV
        /// </summary>
        public const string EXPLOSION_FILEPATH = "Audio/Sfx/explosion.wav";

        /// <summary>
        /// Sound effect played when a <see cref="Entities.Missile"/> is shot.
        /// https://sfxr.me/#34T6PkmBRvhp2cx75ATYSpgQy5qQQae2PKQNReUcESp7j19mSYqEY8cESqFRJXrqPYN9FwCfqxghpw1FYbVFhzGh8j8oZ1gtDbCgT6CodJgDzBuqg91GJnqDH
        /// </summary>
        public const string SHOOT_FILEPATH = "Audio/Sfx/shoot.wav";

        /// <summary>
        /// Sound effect played when receiving damages.
        /// https://sfxr.me/#7BMHBGTvdKADvzBApi1RbXNTgttYsjTXwsWfUQNmcDS8dG8XNDVWxct4BNwmA2zaC2TGhrsYb5DAbRusfgbw7jygyRtgokdbAa5ymVjPdZ1X8dBQ7qD7QJdPV
        /// </summary>
        public const string PLAYER_HIT_FILEPATH = "Audio/Sfx/damages.wav";

        /// <summary>
        /// Sound played when moving around menus.
        /// https://sfxr.me/#3BJdDq819ioQ6877WrLZRHRyQKbbQDGFzFqDd2EXZTfnrEpo7ekjpdZGuf8ub2VihBERMuTKPgwvWWecFbrz1T8LxmewDUrgCXGpmc8kExZe8oPAhFJwn1Y3X
        /// </summary>
        public const string SELECTION_FILEPATH = "Audio/Sfx/select.wav";

        /// <summary>
        /// Music played during gameplay.
        /// https://opengameart.org/content/six-minutes-to-live
        /// </summary>
        public const string IN_GAME_MUSIC_FILEPATH = "Audio/Music/gameplay.wav";

        /// <summary>
        /// Music played in menus.
        /// https://opengameart.org/content/autism-island
        /// </summary>
        public const string MENU_MUSIC_FILEPATH = "Audio/Music/menu.wav";

        /// <summary>
        /// Out device for explosions.
        /// </summary>
        public WaveOut ExplosionSound { get; set; }

        /// <summary>
        /// Out device for player damages.
        /// </summary>
        public WaveOut PlayerHitSound { get; set; }

        /// <summary>
        /// Out device for selections.
        /// </summary>
        public WaveOut SelectionSound { get; set; }

        /// <summary>
        /// Out device for shooting sounds.
        /// </summary>
        public WaveOut ShootSound { get; set; }

        /// <summary>
        /// Out device for in game music.
        /// </summary>
        public WaveOut GameplayMusic { get; set; }

        /// <summary>
        /// Out device for menu music.
        /// </summary>
        public WaveOut MenuMusic { get; set; }

        /// <summary>
        /// Initializes a new <see cref="SoundModel"/>.
        /// </summary>
        public SoundModel()
        {
            GameplayMusic = new WaveOut();
            MenuMusic = new WaveOut();
        }
    }
}
