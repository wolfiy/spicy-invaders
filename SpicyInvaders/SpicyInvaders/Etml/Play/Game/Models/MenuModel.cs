﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 18th, 2024

namespace Ch.Etml.Play.Game.Models
{
    /// <summary>
    /// Model used to hold menu-related data.
    /// </summary>
    public class MenuModel
    {
        /// <summary>
        /// Selected item in the main menu.
        /// </summary>
        public int SelectedMainMenuItem { get; set; }

        /// <summary>
        /// Selected item in the pause menu.
        /// </summary>
        public int SelectedPauseMenuItem { get; set; }

        /// <summary>
        /// Selected item in the settings menu.
        /// </summary>
        public int SelectedSettingsMenuItem { get; set; }

        /// <summary>
        /// Initializes a new <see cref="MenuModel"/>.
        /// </summary>
        public MenuModel() 
        {
            SelectedMainMenuItem = 0;
            SelectedPauseMenuItem = 0;
            SelectedSettingsMenuItem = 0;
        }
    }
}
