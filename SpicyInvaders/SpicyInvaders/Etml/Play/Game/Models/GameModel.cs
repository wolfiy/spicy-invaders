﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 18th, 2024

using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Entities;
using System.Collections.Generic;

namespace Ch.Etml.Play.Game.Models
{
    /// <summary>
    /// Main game model. By default, the difficulty is set to easy.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Default wave the game starts at.
        /// </summary>
        public const int DEFAULT_WAVE = 0;

        /// <summary>
        /// L target framerate
        /// </summary>
        public const int LOW_FRAMERATE = 30;

        /// <summary>
        /// Default target framerate
        /// </summary>
        public const int DEFAULT_FRAMERATE = 48;

        /// <summary>
        /// High target framerate
        /// </summary>
        public const int HIGH_FRAMERATE = 60;

        /// <summary>
        /// Relative path to the score file.
        /// </summary>
        public const string SCORES_FILE = ".\\scores.txt";

        /// <summary>
        /// Whether the player's missile has been shot.
        /// </summary>
        public bool IsMissileShot { get; set; }

        /// <summary>
        /// Whether sound is on or off. By default, sound is on.
        /// </summary>
        public bool IsSoundOn { get; set; }

        /// <summary>
        /// Whether the laser (cheat mode) is enabled.
        /// </summary>
        public bool IsLaserOn { get; set; }

        /// <summary>
        /// Number of wave that have been played through so far.
        /// </summary>
        public int Wave { get; set; }

        /// <summary>
        /// Current target frames per second.
        /// </summary>
        public int Framerate { get; set; }

        /// <summary>
        /// Difficulty of the game.
        /// </summary>
        public Difficulty Difficulty { get; set; }

        /// <summary>
        /// Container used to hold enemy waves.
        /// </summary>
        public EnemyContainer EnemyContainer { get; set; }

        /// <summary>
        /// Main controller.
        /// </summary>
        public GameController Controller { get; set; }

        /// <summary>
        /// Sound controller.
        /// </summary>
        public SoundController SoundController { get; set; }

        /// <summary>
        /// Score.
        /// </summary>
        public ScoreModel Score { get; set; }

        /// <summary>
        /// Contains all in-game entities apart from enemies.
        /// </summary>
        public List<GameObject> GameObjects { get; set; }

        /// <summary>
        /// Player.
        /// </summary>
        public Spaceship Player { get; set; }

        /// <summary>
        /// Current state of the game.
        /// </summary>
        public State State { get; set; }

        /// <summary>
        /// Initianalizes a new <see cref="GameModel"/>.
        /// </summary>
        public GameModel()
        {
            Difficulty = Difficulty.EASY;
            Score = new ScoreModel();
            GameObjects = new List<GameObject>();
            Wave = DEFAULT_WAVE;
            IsLaserOn = false;
            IsSoundOn = true;
            Framerate = DEFAULT_FRAMERATE;
        }
    }
}
