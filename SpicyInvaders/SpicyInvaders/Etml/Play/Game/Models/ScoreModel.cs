﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 18th, 2024

namespace Ch.Etml.Play.Game.Models
{
    /// <summary>
    /// Stores the score.
    /// </summary>
    public  class ScoreModel
    {
        /// <summary>
        /// Current score.
        /// </summary>
        public int Value { get; private set; }

        /// <summary>
        /// Initializes a new <see cref="ScoreModel"/>.
        /// </summary>
        public ScoreModel() 
        { 
            Value = 0;
        }

        /// <summary>
        /// Resets the score.
        /// </summary>
        public void Clear()
        {
            Value = 0;
        }

        /// <summary>
        /// Increments the score by a specified value.
        /// </summary>
        /// <param name="n">Points to add.</param>
        public void Add(int n)
        {
            Value += n;
        }
    }
}
