﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 19th, 2024

// NOTE
// When playback ends, the out device is nulled as the Dispose() method seems
// to hang resulting in the object never being removed from memory.

using Ch.Etml.Play.Game.Models;
using NAudio.Wave;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleToAttribute("SpicyInvaders.Tests")]

namespace Ch.Etml.Play.Game.Controllers
{
    /// <summary>
    /// Handles the game's sounds and musics.
    /// </summary>
    public class SoundController
    {
        /// <summary>
        /// Audio file stream for the explosion sound effect.
        /// </summary>
        private readonly AudioFileReader _explosionSoundFile;

        /// <summary>
        /// Audio file stream for the player hit sound effect.
        /// </summary>
        private readonly AudioFileReader _playerHitSoundFile;

        /// <summary>
        /// Audio file stream for the selection sound effect.
        /// </summary>
        private readonly AudioFileReader _selectionSoundFile;

        /// <summary>
        /// Audio file stream for the shooting sound effect.
        /// </summary>
        private readonly AudioFileReader _shootSoundFile;

        /// <summary>
        /// Audio file stream for the gameplay music.
        /// </summary>
        private readonly AudioFileReader _gameplayMusic;

        /// <summary>
        /// Audio file stream for the menu music.
        /// </summary>
        private readonly AudioFileReader _menuMusic;

        /// <summary>
        /// Sound model.
        /// </summary>
        private readonly SoundModel _soundModel;

        /// <summary>
        /// Game model.
        /// </summary>
        private readonly GameModel _gameModel;

        /// <summary>
        /// Initializes a new <see cref="SoundController"/>.
        /// </summary>
        /// <param name="soundModel">A sound model.</param>
        /// <param name="gameModel">A gome model.</param>
        public SoundController(SoundModel soundModel, GameModel gameModel)
        {
            _soundModel = soundModel;
            _gameModel = gameModel;
            _gameModel.SoundController = this;

            _explosionSoundFile = new AudioFileReader(SoundModel.EXPLOSION_FILEPATH);
            _playerHitSoundFile = new AudioFileReader(SoundModel.PLAYER_HIT_FILEPATH);
            _selectionSoundFile = new AudioFileReader(SoundModel.SELECTION_FILEPATH);
            _shootSoundFile = new AudioFileReader(SoundModel.SHOOT_FILEPATH);
            _gameplayMusic = new AudioFileReader(SoundModel.IN_GAME_MUSIC_FILEPATH);
            _menuMusic = new AudioFileReader(SoundModel.MENU_MUSIC_FILEPATH);
        } // SoundController()

        #region Sound effects
        /// <summary>
        /// Plays a given sound.
        /// </summary>
        /// <param name="sound">The sound to play.</param>
        public void PlaySound(Sound sound)
        {
            WaveOut waveOut;
            AudioFileReader reader;

            switch (sound)
            {
                case Sound.EXPLOSION:
                    waveOut = _soundModel.ExplosionSound;
                    reader = _explosionSoundFile;
                    break;
                case Sound.GAMEPLAY:
                    waveOut = _soundModel.GameplayMusic;
                    reader = _gameplayMusic;
                    break;
                case Sound.MENU:
                    waveOut = _soundModel.MenuMusic;
                    reader = _menuMusic;
                    break;
                case Sound.PLAYER_HIT:
                    waveOut = _soundModel.PlayerHitSound;
                    reader = _playerHitSoundFile;
                    break;
                case Sound.SELECTION:
                    waveOut = _soundModel.SelectionSound;
                    reader = _selectionSoundFile;
                    break;
                case Sound.SHOOT:
                    waveOut = _soundModel.ShootSound;
                    reader = _shootSoundFile;
                    break;
                default:
                    return;
            }

            Play(waveOut, reader);
        }

        /// <summary>
        /// Given an out device and an audio file stream, plays a sound.
        /// </summary>
        /// <param name="waveOut">An out device.</param>
        /// <param name="audioFile">An audio file stream.</param>
        private void Play(WaveOut waveOut, AudioFileReader audioFile)
        {
            // Reset stream and audio device
            audioFile.Position = 0;
            waveOut = null;

            // Create new audio device
            waveOut = new WaveOut();
            waveOut.PlaybackStopped += OutDevice_PlaybackStopped;
            waveOut.Init(audioFile);
            waveOut.Play();
        }

        /// <summary>
        /// Stops a sound.
        /// </summary>
        /// <param name="sound">The type of sound to stop.</param>
        public void StopSound(Sound sound)
        {
            switch (sound)
            {
                case Sound.GAMEPLAY:
                    Stop(_soundModel.GameplayMusic);
                    break;
                case Sound.MENU:
                    Stop(_soundModel.MenuMusic);
                    break;
                default:
                    return;
            }
        }

        /// <summary>
        /// Stops a sound and disposes of its wave out device.
        /// </summary>
        /// <param name="waveOut">Wave device from which the sound is played.
        /// </param>
        private void Stop(WaveOut waveOut)
        {
            waveOut.PlaybackStopped -= OutDevice_PlaybackStopped;
            waveOut.Stop();
            waveOut.Dispose();
        }
        #endregion

        #region Music
        /// <summary>
        /// Plays the gameplay music.
        /// </summary>
        public void PlayGameplayMusic()
        {
            _gameplayMusic.Position = 0;
            _soundModel.GameplayMusic = new WaveOut();
            _soundModel.GameplayMusic.PlaybackStopped += OutDevice_PlaybackStopped;
            _soundModel.GameplayMusic.Init(_gameplayMusic);
            _soundModel.GameplayMusic.Play();
        }

        /// <summary>
        /// Plays the menu music.
        /// </summary>
        public void PlayMenuMusic()
        {
            _menuMusic.Position = 0;
            _soundModel.MenuMusic = new WaveOut();
            _soundModel.MenuMusic.PlaybackStopped += OutDevice_PlaybackStopped;
            _soundModel.MenuMusic.Init(_menuMusic);
            _soundModel.MenuMusic.Play();
        }
        #endregion

        #region Events
        /// <summary>
        /// Disposes of the out device once playback is over. See note for more
        /// details.
        /// </summary>
        /// <param name="sender">An out device.</param>
        /// <param name="e">A playback stopped event.</param>
        public void OutDevice_PlaybackStopped(object sender, EventArgs e)
        {
            WaveOut outDevice = (WaveOut)sender;
            outDevice.PlaybackStopped -= OutDevice_PlaybackStopped;
            outDevice = null;
        }
        #endregion

        #region Getters / setters
        /// <summary>
        /// Gets the current state of the menu music.
        /// </summary>
        /// <returns>True if the menu music is playing, false otherwise.
        /// </returns>
        public bool IsMenuMusicPlaying()
        {
            return _soundModel.MenuMusic.PlaybackState
                   == PlaybackState.Playing;
        }

        /// <summary>
        /// Gets the current state of the gameplay music.
        /// </summary>
        /// <returns>True if the gameplay music is playing, false otherwise.
        /// </returns>
        public bool IsGameplayMusicPlaying()
        {
            return _soundModel.GameplayMusic.PlaybackState
                   == PlaybackState.Playing;
        }
        #endregion
    }
}
