﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 18th, 2024

using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Play.Game.Views;
using Ch.Etml.Utils;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleToAttribute("SpicyInvaders.Tests")]

namespace Ch.Etml.Play.Game.Controllers
{
    /// <summary>
    /// Controller for menus.
    /// </summary>
    public class MenuController
    {
        #region Attributes
        /// <summary>
        /// Main menu.
        /// </summary>
        public MainMenuView MainMenuView { get; }

        /// <summary>
        /// Pause menu.
        /// </summary>
        public PauseMenuView PauseMenuView { get; }

        /// <summary>
        /// Settings menu.
        /// </summary>
        public SettingsMenuView SettingsMenuView { get; }

        /// <summary>
        /// Key that has been pressed by the user.
        /// </summary>
        private ConsoleKey _key;

        /// <summary>
        /// Model used to store menu data.
        /// </summary>
        private MenuModel _model;

        /// <summary>
        /// Controller to use with games (e.g. when starting a new game from
        /// the main menu).
        /// </summary>
        public GameController GameController { get; set; }

        /// <summary>
        /// Sound controller.
        /// </summary>
        public SoundController SoundController { get; set; }
        #endregion

        /// <summary>
        /// Initializes a new <see cref="MenuController"/>.
        /// </summary>
        /// <param name="model">Menu model.</param>
        /// <param name="mainMenu">Main menu view.</param>
        /// <param name="pauseMenu">Pause menu view.</param>
        /// <param name="settings">Settings view.</param>
        public MenuController(MenuModel model, MainMenuView mainMenu,
            PauseMenuView pauseMenu, SettingsMenuView settings)
        {
            _model = model;

            MainMenuView = mainMenu;
            PauseMenuView = pauseMenu;
            SettingsMenuView = settings;

            MainMenuView.Controller = this;
            PauseMenuView.Controller = this;
            SettingsMenuView.Controller = this;
        }

        #region Game
        /// <summary>
        /// Resumes a game.
        /// </summary>
        public void ResumeGame()
        {
            GameController.ResumeGame();
        }

        /// <summary>
        /// Starts a new game.
        /// </summary>
        public void StartGame()
        {
            GameController.StartGame();
        }
        #endregion

        #region Menus
        /// <summary>
        /// Opens the main menu.
        /// </summary>
        public void OpenMainMenu()
        {
            if (GetSoundSetting())
            {
                SoundController.StopSound(Sound.GAMEPLAY);

                if (!SoundController.IsMenuMusicPlaying())
                    SoundController.PlayMenuMusic();
            }

            MainMenuView.Open();
        }

        /// <summary>
        /// Opens the pause menu.
        /// </summary>
        public void OpenPauseMenu()
        {
            PauseMenuView.Open();
        }

        /// <summary>
        /// Opens the settings menu.
        /// </summary>
        public void OpenSettings()
        {
            SettingsMenuView.Open();
        }
        #endregion

        #region User interaction
        /// <summary>
        /// Updates the current item selection based on user input.
        /// </summary>
        /// <param name="view">View from which the user interacts.</param>
        public void UpdateMenuSelection(BaseMenuView view)
        {
            do
            {
                _key = Console.ReadKey(true).Key;
                if (GameController.GetSoundSetting())
                    SoundController.PlaySound(Sound.SELECTION);

                if (_key == ConsoleKey.UpArrow || _key == ConsoleKey.W)
                    view.Selected = Math2.Clamp(0,
                                                view.MenuItems.Length - 1,
                                                --view.Selected);
                else if (_key == ConsoleKey.DownArrow || _key == ConsoleKey.S)
                    view.Selected = Math2.Clamp(0,
                                                view.MenuItems.Length - 1,
                                                ++view.Selected);
                else if (_key == ConsoleKey.Spacebar || _key == ConsoleKey.Enter)
                    view.HandleInput();

                view.Draw();
            }
            while (_key != ConsoleKey.Spacebar
                || _key != ConsoleKey.Enter);
        }
        #endregion

        #region Getters / setters
        /// <summary>
        /// Gets the current game difficulty.
        /// </summary>
        /// <returns>The current game difficulty.</returns>
        public Difficulty GetGameDifficulty()
        {
            return GameController.GetGameDifficulty();
        }

        /// <summary>
        /// Toggles the game difficulty.
        /// </summary>
        /// </param>
        public void ToggleGameDifficulty()
        {
            Difficulty difficulty = GetGameDifficulty();

            switch (difficulty)
            {
                default:
                case Difficulty.EASY:
                    difficulty = Difficulty.MEDIUM;
                    break;
                case Difficulty.MEDIUM:
                    difficulty = Difficulty.HARD;
                    break;
                case Difficulty.HARD:
                    difficulty = Difficulty.EASY;
                    break;
            }

            GameController.SetGameDifficulty(difficulty);
        }

        /// <summary>
        /// Gets the current sound setting.
        /// </summary>
        /// <returns>True if sound is on, false otherwise.</returns>
        public bool GetSoundSetting()
        {
            return GameController.GetSoundSetting();
        }

        /// <summary>
        /// Toggles the game's sound setting.
        /// </summary>
        public void ToggleSoundSetting()
        {
            // Toggle setting
            GameController.SetSoundSetting(!GetSoundSetting());

            // Stop music if playing
            if (!GetSoundSetting())
                SoundController.StopSound(Sound.MENU);
            else
                if (!SoundController.IsMenuMusicPlaying())
                    SoundController.PlayMenuMusic();
        }

        /// <summary>
        /// Gets the current framerate.
        /// </summary>
        /// <returns>The current framerate of the game.</returns>
        public int GetFramerate()
        {
            return GameController.GetFramerate();
        }

        /// <summary>
        /// Sets the target framerate.
        /// </summary>
        public void ToggleFramerate()
        {
            int framerate = GetFramerate();

            switch (framerate)
            {
                case GameModel.LOW_FRAMERATE:
                    framerate = GameModel.DEFAULT_FRAMERATE;
                    break;
                case GameModel.DEFAULT_FRAMERATE:
                    framerate = GameModel.HIGH_FRAMERATE;
                    break;
                case GameModel.HIGH_FRAMERATE:
                    framerate = GameModel.LOW_FRAMERATE;
                    break;
                default:
                    framerate = GameModel.DEFAULT_FRAMERATE;
                    break;
            }

            GameController.SetFramerate(framerate);
        }
        #endregion
    }
}
