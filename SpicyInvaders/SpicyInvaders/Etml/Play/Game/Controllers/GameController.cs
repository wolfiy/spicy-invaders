﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 18th, 2024

using Ch.Etml.IO;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Entities;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Play.Game.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;
using static Ch.Etml.Play.Game.Engine.Constants;

[assembly: InternalsVisibleTo("SpicyInvaders.Tests")]

namespace Ch.Etml.Play.Game.Controllers
{
    /// <summary>
    /// Game engine. Takes care of handling user inputs, updates and draws the 
    /// game.
    /// </summary>
    public class GameController
    {
        #region Attributes
        /// <summary>
        /// One second in milliseconds.
        /// </summary>
        private const double ONE_SECOND = 1000.0;

        /// <summary>
        /// Handles IO with the filesystem.
        /// </summary>
        private readonly FileManager _fileManager;

        /// <summary>
        /// Model used to store the game's data.
        /// </summary>
        private readonly GameModel _model;

        /// <summary>
        /// Main game view.
        /// </summary>
        public GameView GameView { get; private set; }

        /// <summary>
        /// Menu controller.
        /// </summary>
        public MenuController MenuController { get; set; }

        /// <summary>
        /// Sound controller.
        /// </summary>
        public SoundController SoundController { get; set; }
        
        /// <summary>
        /// Default in-game framerate.
        /// </summary>
        private int _framerate = 48;
        #endregion

        /// <summary>
        /// Initializes a new <see cref="GameController"/>.
        /// </summary>
        /// <param name="model">Model containing the game's entities.</param>
        /// <param name="view">Game view used to draw the game.</param>
        public GameController(GameModel model, GameView view)
        {
            _fileManager = new FileManager(GameModel.SCORES_FILE);
            _model = model;
            _model.Controller = this;
            GameView = view;
            GameView.Controller = this;
        }

        #region Game loop
        /// <summary>
        /// Runs the game and starts the main game loop.
        /// </summary>
        public void RunGame()
        {
            // Unpause the game and stop background music
            _model.State = State.RUNNING;
            SoundController.StopSound(Sound.MENU);
            SoundController.StopSound(Sound.GAMEPLAY);
            _framerate = GetFramerate();

            // In-game music
            if (!SoundController.IsGameplayMusicPlaying()
                && GetSoundSetting())
                SoundController.PlayGameplayMusic();

            // Loop as long as the plater is alive.
            while (_model.Player.IsAlive()
                   && _model.State != State.LOSE
                   && _model.State == State.RUNNING)
            {
                // Time at tick
                DateTime frameStartTime = DateTime.Now;

                // Hang loop if game is paused
                if (_model.State != State.PAUSED)
                {
                    HandleUserInput();
                    Update();
                    Draw();
                }

                // Draw at constant framerate
                var frameDuration = DateTime.Now - frameStartTime;
                var targetFrameTime = ONE_SECOND / _framerate;
                var sleepTime
                    = Math.Max(0, (int)(targetFrameTime
                                        - frameDuration.TotalMilliseconds));
                Thread.Sleep(sleepTime);
            }

            EndGame();
            return;
        }

        /// <summary>
        /// Takes and processes user input.
        /// </summary>
        private void HandleUserInput()
        {
            // Prevent key presses from writing to the buffer
            while (Console.KeyAvailable)
                Console.ReadKey(true);

            // Debounce escape key
            if (_model.State == State.PAUSED)
                return;

            // Spaceship movements
            if (Keyboard.IsKeyDown(Key.A) || Keyboard.IsKeyDown(Key.Left))
            {
                if (_model.Player.X > PLAYABLE_BOUND_LEFT)
                    _model.Player.Move(false);
            }
            if (Keyboard.IsKeyDown(Key.D) || Keyboard.IsKeyDown(Key.Right))
            {
                if (_model.Player.X < PLAYABLE_BOUND_RIGHT - _model.Player.GetSpriteSize())
                    _model.Player.Move(true);
            }
            if (Keyboard.IsKeyDown(Key.Space) && (!_model.IsMissileShot || _model.IsLaserOn))
            {
                if (GetSoundSetting())
                    SoundController.PlaySound(Sound.SHOOT);
                _model.Player.Shoot();
                _model.IsMissileShot = true;
            }

            // Pause menu
            if (Keyboard.IsKeyDown(Key.Escape))
            {
                if (_model.State != State.PAUSED)
                {
                    _model.State = State.PAUSED;
                    MenuController.OpenPauseMenu();
                }
            }

            // Debug & cheat
            if (Keyboard.IsKeyDown(Key.M))
                _model.State = State.LOSE;
            if (Keyboard.IsKeyDown(Key.L))
                _model.IsLaserOn = !_model.IsLaserOn;
        }

        /// <summary>
        /// Updates the game logic and entities.
        /// </summary>
        private void Update()
        {
            // Update enemies
            _model.EnemyContainer.Update();

            // Update other game objects
            foreach (GameObject obj in _model.GameObjects.ToList())
            {
                if (obj is Missile && !obj.IsAlive())
                {
                    _model.GameObjects.Remove(obj);
                    if (obj.Side == Side.PLAYER)
                        _model.IsMissileShot = false;
                }

                if (!(obj is Enemy))
                    obj.Update();

                if (!obj.IsAlive() && obj is Bunker)
                    _model.GameObjects.Remove(obj);
            }

            // Respawn enemies when all have been eliminated
            if (!_model.EnemyContainer.IsAlive()
                || _model.EnemyContainer == null)
                SpawnEnemies(ENEMY_SPAWN_POSITION.x,
                             ENEMY_SPAWN_POSITION.y,
                             DEFAULT_ENEMY_WIDTH);

            // End game when player has no hp left
            if (!_model.Player.IsAlive())
                _model.State = State.LOSE;
        }

        /// <summary>
        /// Draws the game.
        /// </summary>
        private void Draw()
        {
            GameView.DrawEntities(_model.GameObjects, _model.EnemyContainer);
            GameView.DrawHud(
                _model.Score.Value, _model.Player.Lives, _model.Wave,
                "Move: <a,d> | Shoot: <space> | Pause: <esc>"
            );
        }
        #endregion

        #region Game setup
        /// <summary>
        /// Resets the game.
        /// </summary>
        public void ResetGame()
        {
            // Clear console and stop music
            Console.Clear();
            SoundController.StopSound(Sound.GAMEPLAY);

            // Playable spaceship.
            _model.Player = new Spaceship(LIVES_SPACESHIP,
                                          INITIAL_PLAYER_POSITION.x,
                                          INITIAL_PLAYER_POSITION.y,
                                          _model);

            // Score
            _model.Score.Clear();
            _model.Wave = 0;
            _model.State = State.PAUSED;

            // In-game entities
            _model.GameObjects.Clear();
            _model.GameObjects.Add(_model.Player);

            // Containers for objects
            SpawnEnemies(ENEMY_SPAWN_POSITION.x,
                         ENEMY_SPAWN_POSITION.y,
                         DEFAULT_ENEMY_WIDTH);

            // Bunkers
            foreach (var (x, y) in DEFAULT_BUNKER_POSITIONS)
            {
                SpawnBunkers(x, y,
                             BUNKER_WIDTH, BUNKER_HEIGHT);
            }
        }

        /// <summary>
        /// Resets and starts the game.
        /// </summary>
        public void StartGame()
        {
            ResetGame();
            RunGame();
        }

        /// <summary>
        /// Resumes the game.
        /// </summary>
        public void ResumeGame()
        {
            foreach (GameObject obj in _model.GameObjects)
                obj.Draw(true);
            RunGame();
        }

        /// <summary>
        /// Handles game end. When the game ends, the game over screen is shown
        /// and the user is prompt to enter their username.
        /// </summary>
        private void EndGame()
        {
            GameView.DrawGameOver();
            ResetGame();
            MenuController.OpenMainMenu();
        }
        #endregion

        #region Spawn entities
        /// <summary>
        /// Spawn a new block of <see cref="Bunkers"/> at the desired location.
        /// </summary>
        /// <param name="x">Horizontal position of the block.</param>
        /// <param name="y">Vertical position of the block.</param>
        /// <param name="width">Width of the block.</param>
        /// <param name="height">Height of the block.</param>
        private void SpawnBunkers(int x, int y, int width, int height)
        {
            for (int i = 0; i < width; ++i)
                for (int j = 0; j < height; ++j)
                    _model.GameObjects.Add(new Bunker(x + i, y + j));
        }

        /// <summary>
        /// Spawn enemies at a given location.
        /// </summary>
        /// <param name="x">Horizontal position, from left to right.</param>
        /// <param name="y">Vertical position, from top to bottom.</param>
        /// <param name="width">Width of the block.</param>
        /// <param name="height">Height of the block.</param>
        private void SpawnEnemies(int x, int y, int width)
        {
            ++_model.Wave;
            _model.EnemyContainer = new EnemyContainer(x, y, width, _model);
            _model.EnemyContainer.Controller = this;
        }
        #endregion

        #region Sounds
        /// <summary>
        /// Plays the specified sound.
        /// </summary>
        /// <param name="sound">Sound to play.</param>
        public void PlaySound(Sound sound)
        {
            SoundController.PlaySound(sound);
        }
        #endregion

        #region IO
        /// <summary>
        /// Adds a score to the scores file.
        /// </summary>
        /// <param name="name">Name of the score holder.</param>
        public void WriteScore(string name)
        {
            _fileManager.WriteScore(name, _model.Score.Value, _model.Wave);
        }
        #endregion

        #region Getters / setters
        /// <summary>
        /// Gets the current game difficulty.
        /// </summary>
        /// <returns>The current game difficulty.</returns>
        public Difficulty GetGameDifficulty()
        {
            return _model.Difficulty;
        }

        /// <summary>
        /// Sets the game's difficulty.
        /// </summary>
        /// <param name="difficulty">Difficulty into which to set the game.
        /// </param>
        public void SetGameDifficulty(Difficulty difficulty)
        {
            _model.Difficulty = difficulty;
        }

        /// <summary>
        /// Gets the current framerate.
        /// </summary>
        /// <returns>The current framerate of the game.</returns>
        public int GetFramerate()
        {
            return _model.Framerate;
        }

        /// <summary>
        /// Sets the target framerate.
        /// </summary>
        /// <param name="fps">Target framerate.</param>
        public void SetFramerate(int fps)
        {
            _model.Framerate = fps;
        }

        /// <summary>
        /// Gets the scores from the file manager.
        /// </summary>
        /// <returns>The scores.</returns>
        public List<(string, int, int)> GetScores()
        {
            return _fileManager.GetScores();
        }

        /// <summary>
        /// Gets the current state of the game's sound setting.
        /// </summary>
        /// <returns>True if sound is enabled, false otherwise.</returns>
        public bool GetSoundSetting()
        {
            return _model.IsSoundOn;
        }

        /// <summary>
        /// Toggles the sound setting of the game.
        /// </summary>
        /// <param name="on">On to turn the sound on, false to disable.</param>
        public void SetSoundSetting(bool on)
        {
            _model.IsSoundOn = on;
        }

        /// <summary>
        /// Sets the game state.
        /// </summary>
        /// <param name="state">State into which to set the game.</param>
        public void SetGameState(State state)
        {
            _model.State = state;
        }
        #endregion
    }
}
