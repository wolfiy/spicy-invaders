﻿/// ETML
/// Author: Sebastien Tille
/// Date: January 18th, 2023

using System.Collections.Generic;
using System;
using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Utils;
using Ch.Etml.Play.Game.Entities;

namespace Ch.Etml.Play.Game.Views
{
    /// <summary>
    /// Represent a Space Invader game.
    /// </summary>
    public class GameView
    {
        /// <summary>
        /// Position of the cursor on the game over user name prompt.
        /// </summary>
        private readonly (int x, int y) USERNAME_CURSOR_POSITION = (29, 11);

        /// <summary>
        /// Main controller.
        /// </summary>
        public GameController Controller { get; set; }

        /// <summary>
        /// Initializes a new <see cref="GameView"/>.
        /// </summary>
        public GameView() {}

        /// <summary>
        /// Draws all in-game entities.
        /// </summary>
        public void DrawEntities(List<GameObject> gameObjects, EnemyContainer enemies)
        {
            // Draw enemies
            enemies.Draw(false);

            // Loop over game objects            
            gameObjects.ForEach(o =>
            {
                // Draw if is alive or is a Missile
                if (o.IsAlive() && !(o is Enemy) || o is Missile)
                    o.Draw(false);
            });
        }

        /// <summary>
        /// Draws the game over screen.
        /// </summary>
        public void DrawGameOver()
        {
            Console.Clear();
            Console2.WriteLineCenterLarge(Constants.GAME_OVER);
            Console.WriteLine();
            Console.WriteLine();

            Console2.WriteLineCenter("Enter your name below:");
            Console.SetCursorPosition(USERNAME_CURSOR_POSITION.x, 
                                      USERNAME_CURSOR_POSITION.y);
            Controller.WriteScore(Console.ReadLine().Trim());
            return;
        }
        
        /// <summary>
        /// Draws the user interface.
        /// </summary>
        /// <param name="score">Current score.</param>
        /// <param name="lives">Number of lives remaining.</param>
        /// <param name="text">Some text.</param>
        public void DrawHud(int score, int lives, int wave, string text)
        {
            var s = $"Score: {score,5} | Lives: {lives} | Wave: {wave} | {text}";
            Console.SetCursorPosition(0, Console.WindowHeight - 1);
            Console.Write(s);
        }
    }
}