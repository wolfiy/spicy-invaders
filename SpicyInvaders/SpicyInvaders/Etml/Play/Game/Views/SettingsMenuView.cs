﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 23rd, 2024

using Ch.Etml.Play.Game.Engine;

namespace Ch.Etml.Play.Game.Views
{
    /// <summary>
    /// Displays the settings menu and gets the user input. The settings lets
    /// the user choose the game's difficulty and toggle audio.
    /// </summary>
    public class SettingsMenuView : BaseMenuView
    {
        /// <summary>
        /// Settings title.
        /// </summary>
        public override string Title => Constants.SETTINGS_MENU;

        /// <summary>
        /// Navigation prompt.
        /// </summary>
        public override string Description => Constants.MAIN_DESCRIPTION;

        /// <summary>
        /// Settings present in the menu.
        /// </summary>
        public override string[] MenuItems => new[] {
            $"Difficulty: {Controller.GetGameDifficulty()}", 
            $"Sound: {Controller.GetSoundSetting()}", 
            $"Framerate: {Controller.GetFramerate()}",
            "Back"
        };

        /// <summary>
        /// Initializes a new <see cref="SettingsMenuView"/>.
        /// </summary>
        /// <param name="menu">Menu from which the settings menu was 
        /// opened.</param>
        public SettingsMenuView() {}

        /// <summary>
        /// Closes the menu.
        /// </summary>
        public override void Close() => Controller.OpenMainMenu();
        

        /// <summary>
        /// Handles user input.
        /// </summary>
        public override void HandleInput()
        {
            switch (Selected)
            {
                case 0:
                    Controller.ToggleGameDifficulty();
                    break;
                case 1:
                    Controller.ToggleSoundSetting();
                    break;
                case 2:
                    Controller.ToggleFramerate();
                    break;
                case 3:
                    Close();
                    break;
                default:
                    break;
            }
        }
    }
}
