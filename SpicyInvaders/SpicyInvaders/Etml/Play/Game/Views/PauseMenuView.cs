﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 21st, 2024

using Ch.Etml.Play.Game.Engine;
using System;

namespace Ch.Etml.Play.Game.Views
{
    /// <summary>
    /// Represents an in-game pause menu. A pause menu lets the user restart, set up or quit the game.
    /// </summary>
    public class PauseMenuView : BaseMenuView
    {
        /// <inheritdoc />
        public override string Title => Constants.PAUSE_TITLE;

        /// <inheritdoc />
        public override string Description => Constants.MAIN_DESCRIPTION;

        /// <inheritdoc />
        public override string[] MenuItems => new[] {
            "Resume", "Restart", "Return to title", "Quit to desktop"
        };

        /// <summary>
        /// Initializes a new <see cref="OldPauseMenuView"/>.
        /// </summary>
        /// <param name="game"><see cref="GameView"/> from which the menu was 
        /// open</param>
        public PauseMenuView() {}

        /// <summary>
        /// Closes the menu and resume the game.
        /// </summary>
        public override void Close()
        {
            Console.Clear();
            Controller.ResumeGame();
        }

        /// <inheritdoc/>
        /// <exception cref="ArgumentException">If the choice does not exist.</exception>
        public override void HandleInput()
        {
            switch (Selected)
            {
                case 0:
                    this.Close();
                    break;
                case 1:
                    Controller.StartGame();
                    this.Close();
                    break;
                case 2:
                    Controller.OpenMainMenu();
                    break;
                case 3:
                    Environment.Exit(0);
                    break;
                default:
                    throw new ArgumentException("Menu item does not exist!");
            }
        }
    }
}
