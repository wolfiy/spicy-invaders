﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 22nd, 2024

using System;
using System.Text;
using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Utils;

namespace Ch.Etml.Play.Game.Views
{
    /// <summary>
    /// Abstract description of a menu and its default behavior.
    /// </summary>
    public abstract class BaseMenuView
    {
        /// <summary>
        /// Controller used to handle menus.
        /// </summary>
        public MenuController Controller { get; set; }

        /// <summary>
        /// Whitespace after a title.
        /// </summary>
        private const int SPACER = 4;

        /// <summary>
        /// Offset for menu items with an odd number of characters.
        /// </summary>
        private const int ODD_ITEM_OFFSET = 1;

        /// <summary>
        /// Title of the menu.
        /// </summary>
        public abstract string Title { get; }

        /// <summary>
        /// Short description of the menu.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Elements to select in the menu.
        /// </summary>
        public abstract string[] MenuItems { get; }

        /// <summary>
        /// Index of the selected menu item.
        /// </summary>
        public int Selected { get; set; }

        /// <summary>
        /// Top offset of the menu items.
        /// </summary>
        public int Offset { get; private set; }

        /// <summary>
        /// Default <see cref="OldBaseMenuView"/> constructor. Displays a title and a description.
        /// </summary>
        public BaseMenuView() { } // Menu()

        public void DrawTitle()
        {
            Console.Clear();
            Selected = 0;
            Offset = Title.Split('\n').Length +
                     Description.Split('\n').Length +
                     SPACER;

            Console2.WriteLineCenterLarge(Title);
            Console2.WriteLineCenter(Description);
        }

        /// <summary>
        /// Closes the menu.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Handles the user inputs.
        /// </summary>
        public abstract void HandleInput();

        /// <summary>
        /// Draws the menu items and highlights the selected item.
        /// </summary>
        public void Draw()
        {
            Console.SetCursorPosition(0, Offset);
            for (int i = 0; i < MenuItems.Length; ++i)
            {
                string item = MenuItems[i];
                int len = item.Length;
                int spacing = (Constants.BUTTON_WIDTH - len) / 2;

                // If the menu item has an odd number of character, the text is
                // not centered. This makes sure that the background remains
                // uniform.
                StringBuilder builder = new StringBuilder();

                if (len % 2 == 0)
                    builder.Append(' ', spacing);
                else
                    builder.Append(' ', spacing + ODD_ITEM_OFFSET);

                builder.Append(item);
                builder.Append(' ', spacing);
                item = builder.ToString();

                if (i == Selected)
                {
                    // Highlight the selected item
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;

                    Console2.WriteLineCenter($"{item}");

                    // Reverting colors to default values
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console2.WriteLineCenter($"{item}");
                }
            }
        } // Draw()

        /// <summary>
        /// Opens the menu.
        /// </summary>
        public void Open()
        {
            DrawTitle();
            Draw();
            Controller.UpdateMenuSelection(this);
        } // Open()
    }
}
