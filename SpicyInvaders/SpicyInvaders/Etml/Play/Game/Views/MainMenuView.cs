﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 21st, 2024

using System;
using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Utils;

namespace Ch.Etml.Play.Game.Views
{
    /// <summary>
    /// Main game menu. Lets the user start a game, change options, get help or quit.
    /// </summary>
    public class MainMenuView : BaseMenuView
    {
        /// <summary>
        /// Width of the player name text.
        /// </summary>
        private const int PLAYER_NAME_WIDTH = 20;

        /// <summary>
        /// Width of the 'Score' text.
        /// </summary>
        private const int SCORE_WIDTH = 5;

        /// <summary>
        /// Width of the 'Wave' text.
        /// </summary>
        private const int WAVE_WIDTH = 4;

        /// <inheritdoc />
        public override string Title => Constants.MAIN_TITLE;

        /// <inheritdoc />
        public override string Description => Constants.MAIN_DESCRIPTION;

        /// <inheritdoc />
        public override string[] MenuItems => new[] { "Start", "Options", "Help", "High scores", "Quit" };

        /// <summary>
        /// Exit the game.
        /// </summary>
        public override void Close()
        {
            Environment.Exit(0);
        }

        /// <inheritdoc/>
        /// <exception cref="ArgumentException">If the choice does not exist.</exception>
        public override void HandleInput()
        {
            switch (Selected)
            {
                case 0:
                    Controller.StartGame();
                    break;
                case 1:
                    Controller.OpenSettings();
                    break;
                case 2:
                    DrawAbout();
                    break;
                case 3:
                    DrawHighScores();
                    break;
                case 4:
                    Close();
                    break;
                default:
                    throw new ArgumentException("Menu item does not exist!");
            }
        }

        /// <summary>
        /// Displays the about section.
        /// </summary>
        private void DrawAbout()
        {
            Console.Clear();
            Console2.WriteLineCenterLarge(Constants.ABOUT_TITLE);
            Console2.WriteLineCenter(Constants.HIGH_SCORES_DESCRIPTION);
            Console.WriteLine();
            Console.WriteLine();

            Console2.WriteLineCenter(
                "Use arrow keys or <a> and <d> to move the spaceship"
            );
            Console2.WriteLineCenter("<space> can be used to shoot.");
            Console.WriteLine();
            Console2.WriteLineCenter(
                "Spicy Invaders is a TUI game developped in C# using"
            );
            Console2.WriteLineCenter(
                ".NET Framework 4.7.2. It was made in 2024 as part of"
            );
            Console2.WriteLineCenter(
                "the P_Dev class at ETML, Lausanne."
            );
            Console.WriteLine();
            Console2.WriteLineCenter(
                "This game is free software. Source code can be found here:"
            );
            Console2.WriteLineCenter(
                "https://gitlab.com/wolfiy/spicy-invaders"
            );

            Console.ReadLine();
            if (Controller.GetSoundSetting())
                Controller.SoundController.PlaySound(Models.Sound.SELECTION);
            Console.Clear();
            Open();
        }

        /// <summary>
        /// Draws the title. As this screen is not interactive, it has not been
        /// moved to a separate class.
        /// </summary>
        private void DrawHighScores()
        {
            var scores = Controller.GameController.GetScores();

            // Header
            Console.Clear();
            Console2.WriteLineCenterLarge(Constants.TITLE_SCORES);
            Console2.WriteLineCenter(Constants.HIGH_SCORES_DESCRIPTION);
            Console.WriteLine();
            Console.WriteLine();

            // Scores TODO const
            Console2.WriteLineCenter($"{"Player",-PLAYER_NAME_WIDTH} | {"Score",SCORE_WIDTH} | {"Wave",WAVE_WIDTH}");
            foreach (var tuple in scores)
                Console2.WriteLineCenter($"{tuple.Item1,-PLAYER_NAME_WIDTH} | {tuple.Item2,5} | {tuple.Item3,4}");

            Console.ReadLine();
            if (Controller.GetSoundSetting())
                Controller.SoundController.PlaySound(Models.Sound.SELECTION);
            Console.Clear();
            Open();
        }
    }
}
