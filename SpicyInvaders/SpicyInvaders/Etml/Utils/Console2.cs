﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 14th, 2024

using System;
using static System.Console;

namespace Ch.Etml.Utils
{
    /// <summary>
    /// Extensions for the <see cref="Console"/> class.
    /// </summary>
    public static class Console2
    {
        /// <summary>
        /// Used to halve values.
        /// </summary>
        private const int HALF = 2;

        /// <summary>
        /// Whitespace that follows large texts.
        /// </summary>
        private const string WHITESPACE = "\n\n";

        /// <summary>
        /// Writes a line of text centered horizontally to the console.
        /// </summary>
        /// <param name="text">Text to center.</param>
        public static void WriteLineCenter(string text)
        {
            int x = WindowWidth / HALF - text.Length / HALF;
            int y = CursorTop;

            SetCursorPosition(x, y);
            WriteLine(text);
        }

        /// <summary>
        /// Writes a multi-line text centered horizontally to the console.
        /// </summary>
        /// <param name="text">A multi-line text.</param>
        public static void WriteLineCenterLarge(string text)
        {
            // Separate each lines
            string[] lines = text.Split('\n');
            int textHeight = lines.Length;

            // Beginning of the line
            int x = WindowWidth / HALF
                  - (text.Length / textHeight) / HALF;
            int y = CursorTop;

            for (int i = 0; i < textHeight; ++i)
            {
                SetCursorPosition(x, y);
                Write(lines[i]);
                ++y;
            }

            // Large texts should always be followed by some white space.
            WriteLine(WHITESPACE);
        }
    }
}
