﻿/// ETML
/// Author: Sebastien TILLE
/// Date: February 21st, 2024

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SpicyInvaders.Tests")]

namespace Ch.Etml.Utils
{
    /// <summary>
    /// Complements to the default Math class.
    /// </summary>
    internal class Math2
    {
        /// <summary>
        /// Clamps a value withing a range.
        /// </summary>
        /// <param name="a">Lower bound.</param>
        /// <param name="b">Upper bound.</param>
        /// <param name="x">Value to clamp.</param>
        /// <returns>If the value is withing the range, returns the value. Otherwise 
        /// returns the lower / upper bound depending on the original value.</returns>
        public static int Clamp(int a, int b, int x)
        {
            if (x < a) return a;
            if (x > b) return b;
            return x;
        }

        /// <inheritdoc cref="Clamp(int, int, int)"/>
        public static double Clamp(double a, double b, double x)
        {
            if (x < a) return a;
            if (x > b) return b;
            return x;
        }
    }
}
