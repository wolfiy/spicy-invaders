﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 30th, 2024

using Ch.Etml.Play.Game.Engine;
using Ch.Etml.Play.Game.Entities;
using Ch.Etml.Play.Game.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch.Etml.Tests.Play.Game.Engine
{
    /// <summary>
    /// Tests the <see cref="Helper"/> class.
    /// </summary>
    [TestClass]
    public class HelperTests
    {
        [TestMethod]
        public void CheckCollisions_CorrectlyDetectsCollisions()
        {
            // Arrange
            var model = new GameModel();
            var m1 = new Missile(0, 0, model, Side.PLAYER);
            var m2 = new Missile(0, 0, model, Side.INVADER);
            var m3 = new Missile(0, 0, model, Side.NEUTRAL);
            var m4 = new Missile(2, 2, model, Side.NEUTRAL);

            // Act
            var actual1 = Helper.CheckCollisions(m1, m2);
            var actual2 = Helper.CheckCollisions(m2, m3);
            var actual3 = Helper.CheckCollisions(m3, m4);
            var actual4 = Helper.CheckCollisions(m4, m1);

            var expected1 = true;
            var expected2 = true;
            var expected3 = false;
            var expected4 = false;

            // Assert
            Assert.AreEqual(expected1, actual1);
            Assert.AreEqual(expected2, actual2);
            Assert.AreEqual(expected3, actual3);
            Assert.AreEqual(expected4, actual4);
        }
    }
}
