﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 30th, 2024

using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch.Etml.Tests.Play.Game.Controllers
{
    /// <summary>
    /// Tests the <see cref="SoundController"/> class.
    /// </summary>
    [TestClass]
    public class SoundControllerTests
    {
        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange
            var soundModel = new SoundModel();
            var gameModel = new GameModel();

            // Act
            var controller = new SoundController(soundModel, gameModel);

            // Assert
            Assert.IsNotNull(controller);
        }
    }
}
