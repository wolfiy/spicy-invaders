﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 30th, 2024

using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Play.Game.Views;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch.Etml.Tests.Play.Game.Controllers
{
    /// <summary>
    /// Tests the <see cref="GameController"/> class.
    /// </summary>
    [TestClass]
    public class GameControllerTests
    {
        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange
            var model = new GameModel();
            var view = new GameView();

            // Act
            var controller = new GameController(model, view);

            // Assert
            Assert.IsNotNull(controller);
        }
    }
}
