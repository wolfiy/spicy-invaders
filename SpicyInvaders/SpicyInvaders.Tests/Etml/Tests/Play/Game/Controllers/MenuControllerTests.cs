﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 30th, 2024

using Ch.Etml.Play.Game.Controllers;
using Ch.Etml.Play.Game.Models;
using Ch.Etml.Play.Game.Views;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch.Etml.Tests.Play.Game.Controllers
{
    /// <summary>
    /// Tests the <see cref="MenuController"/> class.
    /// </summary>
    [TestClass]
    public class MenuControllerTests
    {
        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange
            var model = new MenuModel();
            var main = new MainMenuView();
            var pause = new PauseMenuView();
            var settings = new SettingsMenuView();

            // Act
            var controller = new MenuController(model, main, pause, settings);

            // Assert
            Assert.IsNotNull(controller);
        }
    }
}
