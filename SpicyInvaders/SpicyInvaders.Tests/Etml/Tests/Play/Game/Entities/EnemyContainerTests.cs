﻿using Ch.Etml.Play.Game.Entities;
using Ch.Etml.Play.Game.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ch.Etml.Tests.Play.Game.Entities
{
    /// <summary>
    /// Tests the <see cref=""/>
    /// </summary>
    [TestClass]
    public class EnemyContainerTests
    {
        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange
            var model = new GameModel();
            var x = 0;
            var y = 0;
            var width1 = 5;

            // Act
            var container1 = new EnemyContainer(x, y, width1, model);

            // Assert
            Assert.IsNotNull(container1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ConstructorDoesNotAllowTooSmallBaseWidth()
        {
            // Arrange, act, assert
            var container = new EnemyContainer(0, 0, 0, new GameModel());
        }

        [TestMethod]
        public void ContainerDiesWithEnemies()
        {
            // Arrange
            var container1 = new EnemyContainer(0, 0, 5, new GameModel());
            var container2 = new EnemyContainer(1, 1, 6, new GameModel());

            // Act
            var expected1 = true;
            var expected2 = false;

            foreach (var item in container2.Enemies)
            {
                item.LoseHP();
                item.LoseHP();
                item.LoseHP();
            }

            var actual1 = container1.IsAlive();
            var actual2 = container2.IsAlive();

            // Assert
            Assert.AreEqual(expected1, actual1);
            Assert.AreEqual(expected2, actual2);
        }
    }
}
