﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 30th, 2024

using Ch.Etml.Play.Game.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch.Etml.Tests.Play.Game.Entities
{
    [TestClass]
    public class EnemyTests
    {
        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange, act
            var e1 = new Enemy(0, 0);

            // Assert
            Assert.IsNotNull(e1);
        }
    }
}
