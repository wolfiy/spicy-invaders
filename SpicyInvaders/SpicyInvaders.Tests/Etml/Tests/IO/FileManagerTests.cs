﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 25th, 2024

using Ch.Etml.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ch.Etml.Tests.IO
{
    [TestClass]
    public class FileManagerTests
    {
        private const string TEST_FILE = "./test.txt";

        [TestInitialize]
        public void SetUp() 
        {
            if (File.Exists(TEST_FILE))
                File.Delete(TEST_FILE);
        }

        [TestCleanup]
        public void TearDown()
        {
            if (File.Exists(TEST_FILE))
                File.Delete(TEST_FILE);
        }

        [TestMethod]
        public void Constructor_IsNotNull()
        {
            // Arrange, act
            var fileManager = new FileManager(TEST_FILE);

            // Assert
            Assert.IsNotNull(fileManager);
        }

        [TestMethod]
        [Ignore("Passed, but execution is slow")]
        public void WriteScore_CanWriteToFile()
        {
            // Arrange
            var fileManager = new FileManager(TEST_FILE);
            var username = "username";
            var score = 1000;
            var wave = 4;
            var expected = $"{username},{score},{wave}";

            // Act
            fileManager.WriteScore(username, score, wave);

            // Assert
            var actual = File.ReadAllLines(TEST_FILE)[0];
            Assert.AreEqual(expected, actual);
        }
    }
}
