﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Ch.Etml.Utils.Math2;

namespace Ch.Tests.Utils
{
    [TestClass]
    public class Math2Tests
    {
        #region Integers
        [TestMethod]
        public void Clamp_ReturnsLowerBound()
        {
            // Arrange
            int a = 0;
            int b = 10;
            int x = -100;

            // Act
            int result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(a, result);
        }

        [TestMethod]
        public void Clamp_ReturnsLowerBoundWhenEqual()
        {
            // Arrange
            int a = 0;
            int b = 10;
            int x = 0;

            // Act
            int result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(a, result);
        }

        [TestMethod]
        public void Clamp_ReturnsUpperBound()
        {
            // Arrange
            int a = 0;
            int b = 10;
            int x = 100;

            // Act
            int result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(b, result);
        }

        [TestMethod]
        public void Clamp_ReturnsUpperBoundWhenEqual()
        {
            // Arrange
            int a = 0;
            int b = 10;
            int x = 10;

            // Act
            int result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(b, result);
        }

        [TestMethod]
        public void Clamp_ReturnsValueInBounds()
        {
            // Arrange
            int a = -10000;
            int b = 100;
            int x = -100;

            // Act
            int result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(x, result);
        }
        #endregion

        #region Doubles
        [TestMethod]
        public void Clamp_ReturnsLowerBoundDouble()
        {
            // Arrange
            double a = 0.85;
            double b = 10;
            double x = -100.78;

            // Act
            double result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(a, result);
        }

        [TestMethod]
        public void Clamp_ReturnsLowerBoundWhenEqualDouble()
        {
            // Arrange
            double a = 0.2;
            double b = 10;
            double x = 0.2;

            // Act
            double result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(a, result);
        }

        [TestMethod]
        public void Clamp_ReturnsUpperBoundDouble()
        {
            // Arrange
            double a = 0;
            double b = 11.1111;
            double x = 100;

            // Act
            double result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(b, result);
        }

        [TestMethod]
        public void Clamp_ReturnsUpperBoundWhenEqualDouble()
        {
            // Arrange
            double a = 0;
            double b = 1123.213;
            double x = 1123.213;

            // Act
            double result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(b, result);
        }

        [TestMethod]
        public void Clamp_ReturnsValueInBoundsDouble()
        {
            // Arrange
            double a = -4567.9867;
            double b = 100.82345;
            double x = 4.867435;

            // Act
            double result = Clamp(a, b, x);

            // Assert
            Assert.AreEqual(x, result);
        }
        #endregion
    }
}
